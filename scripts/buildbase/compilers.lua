-- see copyright notice in buildbase/init.lua

local fmt = string.format
local object_ext = '.o'
local executable_fmt = '%s'
local static_archive_fmt = 'lib%s.a'
local shared_object_fmt = 'lib%s.so%s'

if os.name == 'Darwin' then
   shared_object_fmt = 'lib%s%s.dylib'
elseif os.name == 'Windows' then
   object_ext = '.obj'
   executable_fmt = '%s.exe'
   static_archive_fmt = '%s.lib'
   shared_object_fmt = '%s%s.dll'
end

function BUILDER:object_file_name(outdir, srcfile)
   local objfile = srcfile:basename():setext(object_ext)
   return pathcat(outdir, srcfile:hash(true) .. '-' .. objfile)
end

function BUILDER:executable_name(basename)
   return executable_fmt:format(basename)
end

function BUILDER:static_archive_name(basename)
   return static_archive_fmt:format(basename)
end

function BUILDER:shared_object_name(basename, version)
   return shared_object_fmt:format(basename, version)
end

-- mode     - 'debug', 'fast', 'size'
-- runtime  - 'static', 'shared'
function BUILDER:_base_compiler_config(mode, runtime, tool,
                                       major, minor, language)
   return {
      tool_name = tool:basename() .. '-' .. major .. '.' .. minor,

      -- srcfile  - path to input source file
      -- basename - base name of the output artifact
      -- outdir   - path to output directory
      -- arch     - architecture flags

      -- return - {cmd, arch, ...}
      compile_object = function(outfile, arch)
         error('define compile_object')
      end,

      -- return - depsfile, {cmd, arch, ...} or depsfile, nil if compile_object
      --   generates dependences files
      generate_dependence = function(outfile, arch)
         error('define generate_dependence')
      end,

      -- return - {...}
      preprocessor_includes = function(paths)
         error('define preprocessor_include')
      end,

      -- return - {...}
      preprocessor_defines = function(macros)
         error('define preprocessor_defines')
      end,

      -- return - {...}
      preprocessor_undefines = function(macros)
         error('define preprocessor_undefines')
      end,
      
      -- return - {cmd, arch, ...}
      link_static_archive = function(outfile, arch)
         error('define link_static_archive')
      end, 

      -- return - {cmd, arch, ...}
      ranlib_static_archive = function(basename, outdir, arch)
         error('define ranlib_static_archive')
      end, 

      -- return - {cmd, arch, ...}
      link_executable = function(basename, outdir, arch)
         error('define link_executable')
      end,

      -- return - {cmd, arch, ...}
      link_shared_object = function(basename, version, outdir, arch)
         error('define link_shared_object')
      end,

      -- return - {...} 
      link_with_libraries = function(paths, basenames)
         error('define link_with_libraries')
      end,

      archs = {[os.cputype] = {}},
   }
end

local function generate_func_apply(func)
   return function(list)
      if list == nil then
         return {}
      else
         return table.imap(list, func)
      end
   end
end

local function generate_flag_concat(flag)
   return generate_func_apply(function(arg) return flag .. arg end)
end

function BUILDER:_unix_compiler_config(mode, runtime, tool,
                                       major, minor, language)
   local config = self:_base_compiler_config(mode, runtime, tool,
                                             major, minor, language)

   config.preprocessor_includes = generate_flag_concat('-I')
   config.preprocessor_defines = generate_flag_concat('-D')
   config.preprocessor_undefines = generate_flag_concat('-U')

   config.link_executable = function(outfile, arch)
      return {tool, arch, '-o', outfile}
   end

   config.link_static_archive = function(outfile, arch)
      return {'ar', 'cru', outfile}
   end
   config.ranlib_static_archive = function(outfile, arch)
      return {'ranlib', outfile}
   end

   local library_paths = generate_flag_concat('-L')
   local library_libs = generate_flag_concat('-l')
   config.link_with_libraries = function(paths, basenames)
      return {library_paths(paths), library_libs(basenames)}
   end

   return config
end

function BUILDER:_gcc_config(mode, runtime, tool,
                             major, minor, language)
   local config = self:_unix_compiler_config(mode, runtime, tool,
                                             major, minor, language)
   local compile_mode_map = {
      debug = '-g',
      fast = '-O3',
      size = '-Os',
   }
   local compile_runtime_map = {
      static = {},
      shared = '-fPIC',
   }
   mode = assert(compile_mode_map[mode], 'bad mode')
   runtime = assert(compile_runtime_map[runtime], 'bad runtime')

   config.compile_object = function(outfile, arch)
      return {tool, arch, '-c', mode, runtime, '-o', outfile,
              '-MMD', '-MF', outfile:setext('.d') }
   end

   config.generate_dependence = function(outfile, arch)
      return outfile:setext('.d'), nil
   end

   config.link_shared_object = function(outfile, arch)
      local shared_flag = nil
      if os.name == 'Darwin' then
         shared_flag = '-dynamiclib'
      else
         shared_flag = '-shared'
      end
      return {tool, arch, shared_flag, '-o', outfile}
   end

   config.warnings_all = '-Wall'
   config.warnings_as_errors = '-Werror'

   if os.name == 'Darwin' then
      config.archs = {
         --i386 = {'-arch', 'i386'},
         x86_64 = {'-arch', 'x86_64'},
         --powerpc =  {'-arch', 'ppc'},
         --powerpc64 =  {'-arch', 'ppc64'},
         arm64 = {'-arch', 'arm64'},
      }
   end
   return config
end

function BUILDER:_sunpro_config(mode, runtime, tool,
                                major, minor, language)
   local config = self:_unix_compiler_config(mode, runtime, tool,
                                             major, minor, language)

   local compile_mode_map = {
      debug = '-g',
      fast = '-xO5',
   }
   local compile_runtime_map = {
      static = {},
      shared = '-KPIC'
   }
   mode = assert(compile_mode_map[mode], 'bad mode')
   runtime = assert(compile_runtime_map[runtime], 'bad runtime')

   config.compile_object = function(outfile, arch)
      return {tool, arch, '-c', mode, runtime, '-o', outfile}
   end

   config.generate_dependence = nil

   config.link_shared_object = function(outfile, arch)
      return {tool, arch, '-G', '-o', outfile}
   end

   config.warnings_all = {}
   config.warnings_as_errors = {}

   config.archs = {
      sparc = '-xtarget=generic',
      sparcv9 = '-xtarget=generic64',
   }

   return config
end

function BUILDER:_vc_config(mode, runtime, tool,
                            major, minor, language)
   local config = self:_base_compiler_config(mode, runtime, tool,
                                             major, minor, language)

   local compile_mode_map = {
      debug = '-Zi',
      fast = '-Ot',
      size =  '-Os',
   }
   local mt, md = '-MT', '-MD'
   if mode == 'debug' then
      mt, md = '-MTd', '-MDd'
   end

   local compile_runtime_map = {
      static = mt,
      shared = md,
   }
   
   mode = assert(compile_mode_map[mode], 'bad mode')
   runtime = assert(compile_runtime_map[runtime], 'bad runtime')

   config.preprocessor_includes = generate_flag_concat('-I')
   config.preprocessor_defines = generate_flag_concat('-D')
   config.preprocessor_undefines = generate_flag_concat('-U')

   config.compile_object = function(outfile, arch)
      return {tool, arch, '-nologo', '-c', mode, runtime, '-Fo' .. outfile}
   end
   config.generate_dependence = nil

   config.link_executable = function(outfile, arch)
      return {tool, arch, '-nologo', runtime, '-Fe' .. outfile}
   end

   config.link_static_archive = function(outfile, arch)
      return {'lib', '-nologo', '-out:' .. outfile}
   end
   config.ranlib_static_archive = nil

   --config.link_shared_object = function(outfile, arch)
   --   return {tool, arch, '-G', '-o', outfile}
   --end

   local library_paths = generate_flag_concat('-LIBPATH:')
   local library_libs = generate_func_apply(function(arg)
      return self:static_archive_name(arg)
   end)
   config.link_with_libraries = function(paths, basenames)
      return {library_paths(paths), library_libs(basenames)}
   end
   return config
end

function BUILDER:_add_compiler(ext, config)
   self.source_handlers[ext] = function(self, arch, args, input, objdir, tag)
      local output = self:object_file_name(objdir, input)
      local action = self.object_compilers[arch .. ext]
      self:addfile(output, {input}, action, args, tag)
      return output
   end

   local tgav = table.getallvalues
   local function getargs(args, name)
      return config[name](tgav(args, name))
   end
   local function get_extra_libraries(args)
      return config.link_with_libraries(tgav(args, 'extra_library_paths'),
                                        tgav(args, 'extra_libraries'))
   end

   for arch_name,arch in pairs(config.archs) do
      local tag = arch_name .. ext

      self.object_compilers[tag] = function(self, args, output, deps)
         makeoutputdir(output)

         local compiler_flags = tgav(args, 'compiler_flags')
         local preprocessor_flags = tgav(args, 'preprocessor_flags')

         local includes = getargs(args, 'preprocessor_includes')
         local defines = getargs(args, 'preprocessor_defines')
         local undefines = getargs(args, 'preprocessor_undefines')

         local compile_cmd = config.compile_object(output, arch)

         local ret = self:run({compile_cmd, 
                               compiler_flags, preprocessor_flags,
                               includes, defines, undefines,
                               deps[1]})
         if ret == 0 and config.generate_dependence then
            local depsfile, depends_cmd =
                     config.generate_dependence(output, arch)
            if depends_cmd then
               ret = self:run({depends_cmd,
                               compiler_flags, preprocessor_flags,
                               includes, defines, undefines,
                               deps[1]})
            end
            local name = deps[1]:basename():setext(object_ext)
            self:updatedeps(output, depsfile, name)
         end
         return ret
      end

      self.static_linkers[tag] = function(self, args, output, deps)
         makeoutputdir(output)

         local link_cmd = config.link_static_archive(output, arch)
         local ret = self:run({link_cmd, deps})
         if ret == 0 and config.ranlib_static_archive then
            local ranlib_cmd = config.ranlib_static_archive(output, arch)
            ret = self:run(ranlib_cmd)
         end
         return ret
      end

      self.shared_linkers[tag] = function(self, args, output, deps)
         makeoutputdir(output)

         local link_cmd = config.link_shared_object(output, arch)
         local linker_flags = tgav(args, 'linker_flags')
         local library_flags = get_extra_libraries(args)
         return self:run({link_cmd, linker_flags, deps, library_flags})
      end

      self.executable_linkers[tag] = function(self, args, output, deps)
         makeoutputdir(output)

         local link_cmd = config.link_executable(output, arch)
         local linker_flags = tgav(args, 'linker_flags')
         local library_flags = get_extra_libraries(args)
         return self:run({link_cmd, linker_flags, deps, library_flags})
      end
   end
end

function BUILDER:_setup_compiler(mode, runtime, tool,
                                 compilers, extensions, language)
   if not tool then
      error('could not find a ' .. language .. ' compiler')
   end

   local function getconfig(pat, func, stream)
      if stream ~= nil then
         local start, stop, major, minor = stream:find(pat)
         if start and stop and major and minor then
            return func(self, mode, runtime, tool, major, minor, language)
         end
      end
     return nil
   end

   local version_flags = {'--version', '-V'}
   for i = 1, #version_flags do
      local ret, out, err = os.eval({tool, version_flags[i]})
      for j = 1, #compilers do
         local func, pat = unpack(compilers[j])
         local config = getconfig(pat, func, err) or getconfig(pat, func, out)
         if config ~= nil then
            for k = 1, #extensions do
               self:_add_compiler(extensions[k], config)
            end
            return config
         end
      end
   end
   error(tool .. ' not supported')
end

local gcc_cc_pattern = 'gcc.*%(.*%)%s*(%d+)\.(%d+)'
local gcc_cxx_pattern = 'g%+%+.*%(.*%)%s*(%d+)\.(%d+)'
local clang_pattern = 'clang%s+version%s+(%d+)%.(%d+)'
local apple_clang_pattern = 'Apple%s+LLVM%s+version%s+(%d+)%.(%d+)'

local sunpro_cc_pattern = 'cc:%s+Sun%s+C%s+(%d+)%.(%d+)'
local sunpro_cxx_pattern = 'CC:%s+Sun%s+C%+%+%s+(%d+)%.(%d+)'

local vs_pattern =
'Microsoft%s+%(R%).+C/C%+%+%s+Optimizing%s+Compiler%s+Version%s+(%d+)%.(%d+)'

function BUILDER:setup_c_compiler(mode, runtime, tool)
   tool = os.which(os.args.CC or tool or {'clang', 'gcc', 'cl', 'cc'})
   local compilers = {
      {BUILDER._gcc_config, gcc_cc_pattern},
      {BUILDER._gcc_config, clang_pattern},
      {BUILDER._gcc_config, apple_clang_pattern},
      {BUILDER._sunpro_config, sunpro_cc_pattern},
      {BUILDER._vc_config, vs_pattern},
   }
   self.CC = self:_setup_compiler(mode, runtime, tool, compilers,
                                  {'.c'}, 'C')
end

function BUILDER:setup_cplusplus_compiler(mode, runtime, tool)
   tool = os.which(os.args.CXX or tool or {'clang++', 'g++', 'c++', 'cl', 'CC',
                                           'clang', 'gcc', 'cc'})
   local compilers = {
      {BUILDER._gcc_config, gcc_cxx_pattern},
      {BUILDER._gcc_config, clang_pattern},
      {BUILDER._gcc_config, apple_clang_pattern},
      {BUILDER._sunpro_config, sunpro_cxx_pattern},
      {BUILDER._vc_config, vs_pattern},
   }
   self.CXX = self:_setup_compiler(mode, runtime, tool, compilers,
                                   {'.cpp', '.cxx', '.cp'}, 'C++')
end

function BUILDER:setup_objectivec_compiler(mode, runtime, tool)
   tool = os.which(os.args.CC or tool or {'clang', 'gcc'})
   local compilers = {
      {BUILDER._gcc_config, gcc_cc_pattern},
      {BUILDER._gcc_config, clang_pattern},
      {BUILDER._gcc_config, apple_clang_pattern},
   }

   self.OCC = self:_setup_compiler(mode, runtime, tool, compilers,
                                   {'.m'}, 'Objective C')
   self.OCXX = self:_setup_compiler(mode, runtime, tool, compilers,
                                    {'.mm', '.M'}, 'Objective C++')
end

