--Copyright (c) 2008-2011 John A. Hayes
--
--Permission is hereby granted, free of charge, to any person obtaining a copy
--of this software and associated documentation files (the "Software"), to deal
--in the Software without restriction, including without limitation the rights
--to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
--copies of the Software, and to permit persons to whom the Software is
--furnished to do so, subject to the following conditions:
--
--The above copyright notice and this permission notice shall be included in
--all copies or substantial portions of the Software.
--
--THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
--IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
--FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
--AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
--LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
--OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
--THE SOFTWARE.

require('buildbase/utils')
require('buildbase/compilers')
require('buildbase/targets')

function BUILDER:init()
   self.source_handlers = {} -- function(self, arch, args, input, objdir, tag)

   self.object_compilers = {}   -- function(self, args, output, deps)
   self.static_linkers = {}     -- function(self, args, output, deps)
   self.shared_linkers = {}     -- function(self, args, output, deps)
   self.executable_linkers = {} -- function(self, args, output, deps)

   self.archs = { os.cputype }
   self.install_prefix = os.args.prefix or '/usr/local'

   self.combiner = nil -- function that combines multiple architectures
                       -- object files into universal object file
   local lipo = os.which('lipo')
   if lipo ~= nil then
      self.combiner = function(self, args, output, deps)
         makeoutputdir(output)
         return self:run({lipo, deps, '-create', '-output', output})
      end
   end
end

function BUILDER:objdir(arch, ...) error('define objdir()') end
function BUILDER:libdir(arch, ...) error('define libdir()') end
function BUILDER:bindir(arch, ...) error('define bindir()') end

function BUILDER:installdir(...)
   return pathcat(self.install_prefix, ...)
end

