
local function  pathcat(...)
   return table.concat({...}, '/'):cleanup()
end

local seed = ('this is a seed'):hash(true)
local function randpath(path)
   local part = (seed .. path):hash(true)
   seed = part
   return pathcat(path, part)
end

local function randnumber(size)
   seed = (seed .. tostring(size)):hash(true)
   return 1 + seed:hash() % size
end

local function touch(self, args, output, deps)
   os.mkdir(output:dirname())
   local file = io.open(output, 'wt')
   file:write(output)
   file:close()
   return 0
end

local objectdir = 'test-build'
local graph = pathcat(objectdir, 'dag')

function BUILDER:save()
   io.stdout:write('saving graph ...')
   io.stdout:flush()
   self:savegraph({graph}, function(self, target, tag)
      print('deleting: ' .. target)
      os.rmdir(target)
   end)
   io.stdout:write(' done.\n')
   io.stdout:flush()
end

function BUILDER:load()
   local maxdepth = 6
   local maxbranch = 8

   io.stdout:write('loading graph ...')
   io.stdout:flush()
   self:loadgraph({graph})
   io.stdout:write(' done.\n')
   io.stdout:flush()

   local function buildtree(root, depth)
      if depth ~= maxdepth then
         for i = 1, randnumber(maxbranch) do
            local dir = randpath(root)
            buildtree(dir, depth + 1)
            self:addgroup(root, {dir})
         end
      else
         local files = {}
         for i = 1, randnumber(32) do
            local file = randpath(root) .. '.txt'
            self:addfile(file, nil, touch, nil, 1)
            files[#files + 1] = file
         end
         local target = pathcat(root, 'target')
         self:addfile(target, files, touch, nil, 1)
         self:addgroup(root, {target})
      end
   end

   io.stdout:write('building graph ...')
   io.stdout:flush()
   buildtree(objectdir, 0)
   self:addgroup('all', {objectdir})
   io.stdout:write(' done.\n')
   io.stdout:flush()
end


