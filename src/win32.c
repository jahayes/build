/* see copyright notice in common.h */

#include "common.h"
#include <ctype.h>

#define issep(c) (c == '/' || c == '\\')

static double FT2TS(FILETIME *ft)
{
   LONGLONG ll = 0;
   ll |= ft->dwHighDateTime;
   ll <<= 32;
   ll |= ft->dwLowDateTime;
   ll -= 116444736000000000LL;
   return ll * 1e-7;
} 

double file_timestamp(const char *path)
{
   WIN32_FILE_ATTRIBUTE_DATA s;
   if(GetFileAttributesEx(path, GetFileExInfoStandard, &s))
      return FT2TS(&s.ftLastWriteTime);
   else
      return 0;
}

double timestamp_now(void)
{
   FILETIME ft;
   GetSystemTimeAsFileTime(&ft);
   return FT2TS(&ft);
}

int oserror(lua_State *L, const char *msg)
{
   LPTSTR errstr;
   DWORD len = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                             FORMAT_MESSAGE_FROM_SYSTEM |
                             FORMAT_MESSAGE_IGNORE_INSERTS,
                             NULL,
                             GetLastError(),
                             MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                             (LPTSTR)&errstr,
                             0, NULL);
   /* remove the trailing spaces ("\r\n") */
   LPTSTR pos = errstr + len;
   while(pos > errstr && isspace(*pos))
      *pos-- = '\0';

   return luaL_error(L, "%s: %s", msg, errstr);
   /* leak errstr because luaL_error() uses longjmp() */
   /*LocalFree(errstr);*/
}

static int getfullname(const char *path, const char *prog, char *fullpath)
{
   int i;
   static const char *ext[] = { ".cmd", ".bat", ".exe", ".com" };
   for(i = 0; i < ARRAY_LENGTH(ext); i += 1)
   {
      if(SearchPath(path, prog, ext[i], MAXPATH, fullpath, NULL))
         return 1;
   }
   return 0;
}

static int growbuffer(int len, char **buf, int delta)
{
#define SIZE(size) (((size) + 0x1FF) & ~0x1FF)
   /* BUG: memory leak on failure */
   int size = SIZE(len + delta + 2);
   if(size != SIZE(len))
      *buf = realloc(*buf, sizeof(char) * size);
   return *buf == NULL;
#undef SIZE
}

static int loadcmd(lua_State *L, char *path, int *len, char **cmd, int idx)
{
   FLATTEN_NESTED_LIST(loadcmd(L, path, len, cmd, -1))
   {
      size_t l;
      const char *arg = lua_tolstring(L, idx, &l);
      if(arg != NULL)
      {
         const char *space = strchr(arg, ' ');
         if(growbuffer(*len, cmd, l + 2))
            return 1;
         if(*len != 0)
            (*cmd)[(*len)++] = ' ';
         else
            getfullname(getenv("PATH"), arg, path);
         if(space != NULL)
            (*cmd)[(*len)++] = '"';
         memcpy(*cmd + *len, arg, l);
         *len += l;
         if(space != NULL)
            (*cmd)[(*len)++] = '"';
         (*cmd)[(*len)] = '\0';
      }
   }
   return 0;
}

static int doloadenv(lua_State *L, int *len, char **env)
{
   luaL_checktype(L, -1, LUA_TTABLE);
   lua_pushnil(L);
   while(lua_next(L, -2))
   {
      if(lua_istable(L, -1))
      {
         if(doloadenv(L, len, env))
            return 1;
      }
      else
      {
         size_t name_len, value_len;
         const char *name = lua_tolstring(L, -2, &name_len);
         const char *value = lua_tolstring(L, -1, &value_len);
         if(growbuffer(*len, env, name_len + value_len + 2))
            return 1;

         memcpy(*env + *len, name, name_len);
         *len += name_len;
         (*env)[*len] = '=';
         *len += 1;
         memcpy(*env + *len, value, value_len + 1);
         *len += value_len + 1;
         (*env)[*len] = '\0';
      }
      lua_pop(L, 1);
   }
   return 0;
}

static int loadenv(lua_State *L, int *len, char **env)
{
   if(lua_isnoneornil(L, 3))
   {
      lua_settop(L, 2);
      lua_getglobal(L, LUA_OSLIBNAME);
      lua_getfield(L, 3, "env");
      lua_remove(L, 3);
   }
   return doloadenv(L, len, env);
}

HANDLE gProcessHandles[16];
static int gProcessCount = 0;

job_t job_exec(lua_State *L, int echo)
{
   STARTUPINFO si;
   PROCESS_INFORMATION pi;

   int len;
   char path[MAXPATH];
   char *cmd = NULL;
   char *env = NULL;
   job_t ret = JOB_NULL;
   const char *workingdir = luaL_optstring(L, 2, NULL);

   if(gProcessCount == ARRAY_LENGTH(gProcessHandles))
      return JOB_NULL;

   len = 0;
   if(loadcmd(L, path, &len, &cmd, 1))
      return JOB_NULL;

   len = 0;
   if(loadenv(L, &len, &env))
      goto cleanup;

   if(echo)
   {
      if(workingdir != NULL)
         message("(%s)", workingdir);
      message(">> %s\n", cmd);
   }
   
   ZeroMemory(&si, sizeof(si));
   si.cb = sizeof(si);
   ZeroMemory(&pi, sizeof(pi));
   if(CreateProcess(path, cmd, NULL, NULL, FALSE,
                    0, env, workingdir, &si, &pi))
   {
      CloseHandle(pi.hThread);
      ret = pi.hProcess;
      gProcessHandles[gProcessCount++] = ret;
   }

   free(env);
cleanup:
   free(cmd);
   return ret;
}

job_t job_wait(job_t job, int *ret, int block)
{
   DWORD exitcode;
   DWORD timeout = block ? INFINITE : 0;
   if(job != JOB_NULL)
   {
      int idx = 0;
      if(WaitForSingleObject(job, timeout) != 0)
         return JOB_NULL;
      while(idx < gProcessCount && gProcessHandles[idx] != job)
          idx += 1;
      gProcessHandles[idx] = gProcessHandles[--gProcessCount];
   }
   else if(gProcessCount > 0)
   {
      DWORD idx = WaitForMultipleObjects(gProcessCount,
                                         gProcessHandles,
                                         FALSE, timeout);
      if(WAIT_OBJECT_0 <= idx && idx < WAIT_OBJECT_0 + gProcessCount)
      {
         idx -= WAIT_OBJECT_0;
         job = gProcessHandles[idx];
         gProcessHandles[idx] = gProcessHandles[--gProcessCount];
      }
      else
         return JOB_NULL;
   }
   else
      return JOB_NULL;

    
   if(GetExitCodeProcess(job, &exitcode))
   {
      *ret = exitcode;
      CloseHandle(job);
      return job;
   }
   else
   {
      CloseHandle(job);
      return JOB_NULL;
   }
}

static char *nextslash(char *path)
{
   char *ret;
   while(issep(*path))
      path += 1;
   ret = strchr(path, '/');
   if(ret == NULL)
      ret = strchr(path, '\\'); 
   return ret;
}

int make_directory(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(make_directory(L, -1))
   {
      DWORD attr;
      char path[MAXPATH];
      {
         size_t len = 0;
         copylstring(L, idx, len, path);
         while(issep(path[len - 1]))
            path[--len] = '\0';
      }
      if((attr = GetFileAttributes(path)) == INVALID_FILE_ATTRIBUTES)
      {
         /* walk the parts of the path */
         char *pos = path;
         while((pos = nextslash(pos)) != NULL)
         {
            *pos = '\0';
            if(!CreateDirectory(path, NULL) &&
               GetLastError() != ERROR_ALREADY_EXISTS)
               return 1;
            *pos = '/';
         }
         if(!CreateDirectory(path, NULL) &&
            GetLastError() != ERROR_ALREADY_EXISTS)
            return 1;
      }
   }
   return 0;
}

static int removeobject(char *path)
{
   DWORD attr;
   int err = 0;
   if((attr = GetFileAttributes(path)) != INVALID_FILE_ATTRIBUTES)
   {
      if(attr & FILE_ATTRIBUTE_DIRECTORY)
      {
         HANDLE dir;
         WIN32_FIND_DATA fileData;
         char *pos = path + strlen(path);
         pos[0] = '/';
         pos[1] = '*';
         pos[2] = '\0';
         dir = FindFirstFile(path, &fileData);
         do
         {
#define DOTDOT(s) \
   ((s)[0] == '.' && ((s)[1] == '\0' || ((s)[1] == '.' && (s)[2] == '\0')))
            if(!DOTDOT(fileData.cFileName))
            {
               strcpy(pos + 1, fileData.cFileName);
               err += removeobject(path);
            }
         }
         while(FindNextFile(dir, &fileData));
         FindClose(dir);
         pos[0] = '\0';
         if(!RemoveDirectory(path))
            err += 1;
      }
      else
      {
         if(!DeleteFile(path))
            err += 1;
      }
   }
   return err;
}

static char *lastslash(char *path)
{
   char *end;
   // don't return first / of an absolute path
   if(issep(path[0]) && issep(path[1]))     /* UNC paths */
      path += 1;
   else if(isalpha(path[0]) && path[1] == ':')  /* drives */
      path += 2;
   while(issep(*path))
      path += 1;

   end = strrchr(path, '/');
   if(end == NULL)
      end = strrchr(path, '\\');
   
   if(end != NULL)
   {
      while(path < end && issep(end[-1]))
         end -= 1;
   }
   return end;
}

int remove_directory(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(remove_directory(L, -1))
   {
      char *end, path[MAXPATH];
      {
         size_t len = 0;
         copylstring(L, idx, len, path);
         while(issep(path[len - 1]))
            path[--len] = '\0';
      }
      if(removeobject(path))
         return 1;
      
      while((end = lastslash(path)) != NULL)
      {
         *end = '\0';
         if(!RemoveDirectory(path))
            break;
      }
   }
   return 0;
}

static void convert_slashes(char *path)
{
   while(*path != '\0')
   {
      if(issep(*path))
         *path = '/';
      path += 1;
   }
}

int find_first_program(lua_State *L, const char *path, char *full, int idx)
{
   FLATTEN_NESTED_LIST(find_first_program(L, path, full, -1))
   {
      const char *prog = lua_tostring(L, idx);
      if(getfullname(path, prog, full))
      {
         convert_slashes(full);
         return 1;
      }
   }
   return 0;
}

int glob_pattern(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(glob_pattern(L, -1))
   {
      return luaL_error(L, "glob -- not implemented");
   }
   return 0;
}

static BOOL MyCreatePipe(HANDLE pipe[2])
{
   HANDLE temp;

   SECURITY_ATTRIBUTES sa;
   ZeroMemory(&sa, sizeof(sa));
   sa.nLength= sizeof(sa);
   sa.lpSecurityDescriptor = NULL;
   sa.bInheritHandle = TRUE;

   if(!CreatePipe(&temp, pipe + 1, &sa, 0))
      return FALSE;
   if(!DuplicateHandle(GetCurrentProcess(),
                       temp,
                       GetCurrentProcess(),
                       pipe + 0,
                       0,
                       FALSE,
                       DUPLICATE_SAME_ACCESS))
      return FALSE;
   CloseHandle(temp);
   return TRUE;
}

static DWORD readpipe(lua_State *L, HANDLE pipe[2], luaL_Buffer *str)
{
   DWORD count, total = 0;
   char buffer[256];

   CloseHandle(pipe[1]);
   luaL_buffinit(L, str);
   while(ReadFile(pipe[0], buffer, sizeof(buffer), &count, NULL) &&
         count != 0)
   {
      luaL_addlstring(str, buffer, count);
      total += count;
   }
   CloseHandle(pipe[0]);
   return total;
}

int os_eval(lua_State *L)
{
   DWORD status;
   STARTUPINFO si;
   HANDLE out[2], err[2];
   PROCESS_INFORMATION pi;
   luaL_Buffer outstr, errstr;
   DWORD outlen, errlen;
   DWORD exitcode;

   int len;
   char path[MAXPATH];
   char *cmd = NULL;
   char *env = NULL;
   const char *workingdir = luaL_optstring(L, 2, NULL);

   len = 0;
   if(loadcmd(L, path, &len, &cmd, 1))
      return oserror(L, "loadcmd()");
   
   len = 0;
   if(loadenv(L, &len, &env))
   {
      free(cmd);
      return oserror(L, "loadenv()");
   }

   if(!MyCreatePipe(out) || !MyCreatePipe(err))
   {
      free(env);
      free(cmd);
      return oserror(L, "CreatePipe()");
   }

   ZeroMemory(&si, sizeof(si));
   si.cb = sizeof(si);
   si.dwFlags = STARTF_USESTDHANDLES;
   si.hStdInput = GetStdHandle(STD_INPUT_HANDLE);
   si.hStdOutput = out[1];
   si.hStdError = err[1];

   ZeroMemory(&pi, sizeof(pi));
   status = CreateProcess(path, cmd, NULL, NULL, TRUE,
                          0, env, workingdir, &si, &pi);
   free(env);
   free(cmd);
   if(!status)
      return oserror(L, "CreateProcess()");
   CloseHandle(pi.hThread);

   outlen = readpipe(L, out, &outstr);
   errlen = readpipe(L, err, &errstr);

   status = WaitForSingleObject(pi.hProcess, INFINITE);
   if(status == WAIT_OBJECT_0)
      GetExitCodeProcess(pi.hProcess, &exitcode);

   CloseHandle(pi.hProcess);
   if(status != WAIT_OBJECT_0)
      return oserror(L, "WaitForSingleObject()");

   lua_pushinteger(L, exitcode);
   if(outlen > 0)
      luaL_pushresult(&outstr);
   else
      lua_pushnil(L);
   if(errlen > 0)
      luaL_pushresult(&errstr);
   else
      lua_pushnil(L);

   return 3;
}

/* os.cwd() */
int os_cwd(lua_State *L)
{
   char path[MAXPATH];
   DWORD ret = GetCurrentDirectory(MAXPATH, path);
   if(ret == 0 || ret > MAXPATH)
      return oserror(L, "GetCurrentDirectory()");
   convert_slashes(path);
   lua_pushstring(L, path);
   return 1;
}
