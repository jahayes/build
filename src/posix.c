/* see copyright notice in common.h */

#include "common.h"

#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/time.h>
#include <dirent.h>
#include <glob.h>

#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>

#define issep(c) ((c) == '/')

double file_timestamp(const char *path)
{
   double ts = 0;
   struct stat s;
   if(stat(path, &s) == 0)
      ts = s.st_mtime;
   return ts;
}

double timestamp_now(void)
{
   double ts;
   struct timeval tv;
   if(gettimeofday(&tv, NULL) != 0)
      return 0;
   ts = tv.tv_sec;
   ts += tv.tv_usec * 1e-6;
   return ts;
}

int oserror(lua_State *L, const char *msg)
{
   return luaL_error(L, "%s: %s", msg, strerror(errno));
}

static int growlist(int argc, char ***argv)
{
#define SIZE(size) (size_t)(((size) + 0xFF) & ~0xFF)
   /* BUG: memory leak on failure */
   if(SIZE(argc + 2) != SIZE(argc))
      *argv = realloc(*argv, sizeof(char *) * SIZE(argc + 2));
   return *argv == NULL;
#undef SIZE
}

static int loadargs(lua_State *L, int *argc, char ***argv, int idx)
{
   FLATTEN_NESTED_LIST(loadargs(L, argc, argv, -1))
   {
      const char *arg = lua_tostring(L, idx);
      if(arg != NULL)
      {
         growlist(*argc, argv);
         (*argv)[(*argc)++] = (char *)arg;
         (*argv)[(*argc)] = NULL;
      }
   }
   return 0;
}

static int doloadenv(lua_State *L, int *envc, char ***envv)
{
   luaL_checktype(L, -1, LUA_TTABLE);
   lua_pushnil(L);
   while(lua_next(L, -2))
   {
      if(lua_istable(L, -1))
      {
         if(doloadenv(L, envc, envv))
            return 1;
      }
      else
      {
         size_t name_len, value_len;
         const char *name = lua_tolstring(L, -2, &name_len);
         const char *value = lua_tolstring(L, -1, &value_len);
         char *str = calloc(name_len + value_len + 2, sizeof(char));
         memcpy(str, name, name_len);
         str[name_len] = '=';
         memcpy(str + name_len + 1, value, value_len + 1);

         growlist(*envc, envv);
         (*envv)[(*envc)++] = str;
         (*envv)[(*envc)] = NULL;
      }
      lua_pop(L, 1);
   }
   return 0;
}

static int loadenv(lua_State *L, int *envc, char ***envv)
{
   if(lua_isnoneornil(L, 3))
   {
      lua_settop(L, 2);
      lua_getglobal(L, LUA_OSLIBNAME);
      lua_getfield(L, 3, "env");
      lua_remove(L, 3);
   }
   return doloadenv(L, envc, envv);
}

static void freeenv(char **envv)
{
   if(envv != NULL)
   {
      int i = 0;
      while(envv[i] != NULL)
         free(envv[i++]);
      free(envv);
   }
}

static int count_signals = 0;
static sigset_t s_mask;
static struct sigaction s_sigint;
static struct sigaction s_sigquit;

static void setupsignals(void)
{
   {
      struct sigaction ignore;
      ignore.sa_handler = SIG_IGN;
      sigemptyset(&ignore.sa_mask);
      ignore.sa_flags = 0;
      
      sigaction(SIGINT, &ignore, &s_sigint);
      sigaction(SIGQUIT, &ignore, &s_sigquit);
   }
   {
      sigset_t block;
      sigemptyset(&block);
      sigaddset(&block, SIGCHLD);
      sigprocmask(SIG_BLOCK, &block, &s_mask);
   }
}

static void cleanupsignals(void)
{
   sigprocmask(SIG_SETMASK, &s_mask, NULL);
   sigaction(SIGQUIT, &s_sigint, NULL);
   sigaction(SIGINT, &s_sigquit, NULL);
}

extern char **environ;
job_t job_exec(lua_State *L, int echo)
{
   pid_t pid = -1;
   int argc = 0, envc = 0;
   char **argv = NULL, **envv = NULL;
   const char *workingdir = luaL_optstring(L, 2, NULL);

   if(loadargs(L, &argc, &argv, 1))
      return JOB_NULL;
   if(loadenv(L, &envc, &envv))
      return JOB_NULL;

   if(echo)
   {
      int i;
      if(workingdir != NULL)
         message("(%s)", workingdir);
      message(">>");
      for(i = 0; i < argc; i += 1)
      {
         const char *str = argv[i];
         message(" ");
         while(*str != '\0')
         {
            if(*str == '"' || *str == '\'' || *str == ' ' || *str == '\\')
               message("\\");
            message("%c", *str);
            str++;
         }
      }
      message("\n");
   }

   if(count_signals++ == 0)
      setupsignals();
   
   fflush(stdout);
   if((pid = fork()) == 0)
   {
      cleanupsignals();

      if(workingdir != NULL && chdir(workingdir) != 0)
         _exit(EXIT_FAILURE);

      environ = envv;
      execvp(argv[0], argv);
      _exit(EXIT_FAILURE);
   }
   else
   {
      free(argv);
      freeenv(envv);
      return pid;
   }
}

job_t job_wait(job_t job, int *ret, int block)
{
   pid_t pid;
   while((pid = waitpid(job, ret, block ? 0 : WNOHANG)) == -1)
   {
      if(errno != EINTR)
         return JOB_NULL;
   }

   if(pid == 0)
      return JOB_NULL;

   if(--count_signals == 0)
      cleanupsignals();

   if(WIFEXITED(*ret))
      *ret = WEXITSTATUS(*ret);
   return pid;
}

static char *nextslash(char *path)
{
   while(issep(*path))
      path += 1;
   return strchr(path, '/');
}

#define NEW_FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
#define NEW_DIR_MODE (NEW_FILE_MODE | S_IXUSR | S_IXGRP | S_IXOTH)
int make_directory(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(make_directory(L, -1))
   {
      struct stat s;
      char path[MAXPATH];
      {
         size_t len = 0;
         copylstring(L, idx, len, path);
         while(issep(path[len - 1]))
            path[--len] = '\0';
      }
      if(stat(path, &s) == -1)
      {
         /* walk the parts of the path */
         char *pos = path;
         while((pos = nextslash(pos)) != NULL)
         {
            *pos = '\0';
            if(stat(path, &s) == -1)
            {
               if(mkdir(path, NEW_DIR_MODE) == -1)
                  return 1;
            }
            *pos = '/';
         }
         if(mkdir(path, NEW_DIR_MODE) == -1)
            return 1;
      }
   }
   return 0;
}

static int removeobject(char *path, struct stat *s)
{
   int err = 0;
   if(lstat(path, s) == 0)
   {
      if(S_ISDIR(s->st_mode))
      {
         struct dirent *ent;
         DIR *dir = opendir(path);
         char *pos = path + strlen(path);
         pos[0] = '/';
         while((ent = readdir(dir)) != NULL)
         {
#define DOTDOT(s) \
   ((s)[0] == '.' && ((s)[1] == '\0' || ((s)[1] == '.' && (s)[2] == '\0')))
            if(!DOTDOT(ent->d_name))
            {
               strcpy(pos + 1, ent->d_name);
               err += removeobject(path, s);
            }
         }
         closedir(dir);
         pos[0] = '\0';
         if(rmdir(path) == -1)
            err += 1;
      }
      else
      {
         if(unlink(path) == -1)
            err += 1;
      }
   }
   return err;
}

static char *lastslash(char *path)
{
   char *end;
   /* don't return first / of an absolute path */
   while(issep(*path))
      path += 1;
   if((end = strrchr(path, '/')) != NULL)
   {
      while(path < end && issep(end[-1]))
         end -= 1;
   }
   return end;
}

int remove_directory(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(remove_directory(L, -1))
   {
      struct stat s;
      char *end, path[MAXPATH];
      {
         size_t len = 0;
         copylstring(L, idx, len, path);
         while(issep(path[len - 1]))
            path[--len] = '\0';
      }
      if(removeobject(path, &s))
         return 1;
      
      while((end = lastslash(path)) != NULL)
      {
         *end = '\0';
         if(rmdir(path) == -1)
            break;
      }
   }
   return 0;
}


int find_first_program(lua_State *L, const char *path, char *full, int idx)
{
   FLATTEN_NESTED_LIST(find_first_program(L, path, full, -1))
   {
      size_t len = 0;
      struct stat s;
      const char *prog = lua_tolstring(L, idx, &len);
      
      if(strchr(prog, '/') != NULL)
      {
         memcpy(full, prog, len + 1);
         if(access(full, X_OK) == 0 && stat(full, &s) == 0 &&
            (s.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH)) != 0)
            return 1;
      }
      
      while(*path != '\0')
      {
         size_t length = 0;
         {
            const char *tail = strchr(path, ':');
            if(tail == NULL)
               length = strlen(path);
            else
               length = (size_t)(tail - path);
         }
         
         memcpy(full, path, length);
         full[length] = '/';
         memcpy(full + length + 1, prog, len + 1);
         
         if(access(full, X_OK) == 0 && stat(full, &s) == 0 &&
            (s.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH)) != 0)
            return 1;
         
         path += length;
         while(*path == ':')
            path += 1;
      }
   }
   return 0;
}

int glob_pattern(lua_State *L, int idx)
{
   FLATTEN_NESTED_LIST(glob_pattern(L, -1))
   {
      glob_t g;
      const char *pattern = luaL_checkstring(L, idx);
      memset(&g, 0, sizeof(g));
      if(glob(pattern, GLOB_BRACE, NULL, &g) == 0)
      {
         size_t i, last = lua_objlen(L, 1) + 1;
         for(i = 0; i < g.gl_pathc; i += 1)
         {
            lua_pushstring(L, g.gl_pathv[i]);
            lua_rawseti(L, 1, (int)(last + i));
         }
         globfree(&g);
      }
   }
   return 0;
}

static ssize_t readpipe(lua_State *L, int fd[2], luaL_Buffer *str)
{
   ssize_t count, total = 0;
   char buffer[256];
   
   close(fd[1]);
   luaL_buffinit(L, str);
   while((count = read(fd[0], buffer, sizeof(buffer))) != 0)
   {
      if(count == -1)
      {
         if(errno != EAGAIN && errno != EINTR)
            return -1;
      }
      else
      {
         luaL_addlstring(str, buffer, (size_t)count);
         total += count;
      }
   }
   close(fd[0]);
   return total;
}

static void redirectpipe(int fd[2], int stdio)
{
   if(fd[1] != stdio)
   {
      dup2(fd[1], stdio);
      close(fd[1]);
   }
   close(fd[0]);
}

/* os.run(argv (1)[, workingdir (2)[, env(3)]]) */
int os_eval(lua_State *L)
{
   int out[2], err[2];
   pid_t pid = -1;
   int argc = 0, envc = 0;
   char **argv = NULL, **envv = NULL;
   const char *workingdir = luaL_optstring(L, 2, NULL);

   if(loadargs(L, &argc, &argv, 1))
      return oserror(L, "loadargs()");
   if(loadenv(L, &envc, &envv))
      return oserror(L, "loadenv()");

   if(count_signals++ == 0)
      setupsignals();

   if(pipe(out) == -1 || pipe(err) == -1)
   {
      free(argv);
      freeenv(envv);
      return oserror(L, "pipe()");
   }

   if((pid = fork()) == 0)
   {
      cleanupsignals();
      redirectpipe(out, STDOUT_FILENO);
      redirectpipe(err, STDERR_FILENO);

      if(workingdir != NULL && chdir(workingdir) != 0)
         _exit(EXIT_FAILURE);

      environ = envv;
      execvp(argv[0], argv);
      _exit(EXIT_FAILURE);
   }
   else
   {
      free(argv);
      freeenv(envv);
      if(pid == -1)
      {
         close(err[0]);
         close(err[1]);
         close(out[0]);
         close(out[1]);
         return oserror(L, "fork()");
      }
      else
      {
         int ret = 0;
         luaL_Buffer outstr, errstr;
         ssize_t outlen = readpipe(L, out, &outstr);
         ssize_t errlen = readpipe(L, err, &errstr);
         while(waitpid(pid, &ret, 0) == -1)
         {
            if(errno != EINTR)
               return oserror(L, "waitpid()");
         }
         
         lua_pushinteger(L, ret);
         if(outlen > 0)
            luaL_pushresult(&outstr);
         else
            lua_pushnil(L);
         if(errlen > 0)
            luaL_pushresult(&errstr);
         else
            lua_pushnil(L);
         
         if(--count_signals == 0)
            cleanupsignals();
         return 3;
      }
   }
}

/* os.cwd() */
int os_cwd(lua_State *L)
{
   char path[MAXPATH];
   if(getcwd(path, MAXPATH) == NULL)
      return oserror(L, "getcwd()");
   lua_pushstring(L, path);
   return 1;
}

/* os.stat(path (1)) */
int os_stat(lua_State *L)
{
   const char *path = luaL_checkstring(L, 1);
   struct stat s;
   if(stat(path, &s) == 0)
   {
      lua_createtable(L, 0, 10);

      char mode[9];

#define S_ISOID 0
#define SETMODE(idx, mask, value) \
   mode[(idx)] = (s.st_mode & (mask)) ? value : '-' 
#define DOMODE(idx, tag, sticky) \
   SETMODE((idx) + 0, S_IR##tag, 'r'); \
   SETMODE((idx) + 1, S_IW##tag, 'w'); \
   SETMODE((idx) + 2, S_IX##tag, 'x'); \
   if(s.st_mode & S_IS##sticky) \
      mode[(idx) + 2] = (s.st_mode & S_IX##tag) ? 's' : 'S'

      DOMODE(0, USR, UID);
      DOMODE(3, GRP, GID);
      DOMODE(6, OTH, OID);
      
#undef S_ISOID
#undef SETMODE
#undef DOMODE
      lua_pushlstring(L, mode, 9);
      lua_setfield(L, -2, "mode");

      if (S_ISREG(s.st_mode))
         lua_pushliteral(L, "regular");
      else if (S_ISLNK(s.st_mode))
         lua_pushliteral(L, "link");
      else if (S_ISDIR(s.st_mode))
         lua_pushliteral(L, "directory");
      else if (S_ISCHR(s.st_mode))
         lua_pushliteral(L, "character device");
      else if (S_ISBLK(s.st_mode))
         lua_pushliteral(L, "block device");
      else if (S_ISFIFO(s.st_mode))
         lua_pushliteral(L, "fifo");
      else if (S_ISSOCK(s.st_mode))
         lua_pushliteral(L, "socket");
      else
         lua_pushliteral(L, "?");
      lua_setfield(L, -2, "type");

      lua_pushnumber(L, s.st_size);
      lua_setfield(L, -2, "size");

      lua_pushnumber(L, s.st_atime);
      lua_setfield(L, -2, "atime");

      lua_pushnumber(L, s.st_mtime);
      lua_setfield(L, -2, "mtime");

      lua_pushnumber(L, s.st_ctime);
      lua_setfield(L, -2, "ctime");
   }
   else
      lua_pushnil(L);
   return 1;
}


