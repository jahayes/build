
_build()
{
   case ${COMP_WORDS[COMP_CWORD-1]} in
      -) COMPREPLY=(-q -e -r -v -c -t -f -j -S -U -A -B -D -M -H) ;;
      *) COMPREPLY=( $("${COMP_WORDS[@]}" -C | awk '/^\*C\*/{print $2}') ) ;;
   esac
}
complete -F _build build



