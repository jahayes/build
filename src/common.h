/******************************************************************************
* Copyright (c) 2008-2011 John A. Hayes
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

#ifndef __COMMON_H__
#define __COMMON_H__

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include <stdlib.h>

void message(const char *fmt, ...);
void error(const char *fmt, ...);

void dump_stack(lua_State *L);
void register_functions(lua_State *L, const char *table,
                         const struct luaL_reg *funcs, int debug);
void register_methods(lua_State *L, const char *metatable,
                      const struct luaL_reg *funcs, int debug);

void warn_undefined_globals(lua_State *L);

/* Load "--KEY=VALUE" command line args into os.args table */
void load_args(lua_State *L, int argc, const char *argv[], int debug);
void load_env(lua_State *L, const char *argv[], int debug);
void update_packagepath(lua_State *L, int debug);

void append_os(lua_State *L, int debug);
void append_io(lua_State *L, int debug);
void append_table(lua_State *L, int debug);
void append_string(lua_State *L, int debug);

/* Loads needed functions and the builder class into the Lua VM */
void open_builder(lua_State *L, int debug);

void open_templates(lua_State *L, int debug);

int call_hook(lua_State *L, const char *name, int debug);


/* command line parsing functions */
const char *find_arg(int argc, const char *argv[],
                     int opt, const char *value);
int find_flag(int argc, const char *argv[], int opt);
int compact_args(int argc, const char *argv[]);

/*
 Removes '/../', '/./' and '//' -- making it easier to read
 normalize_path() can turn invalid paths into valid paths.
 
 normalize_path("dir1/not_a_dir/../dir2") ==> "dir1/dir2"

 Calling the shell's cd on 'dir1/not_a_dir/../dir2' will fail but when 
 called with 'dir1/dir2' it will pass.

 input may equal output
 */
char *normalize_path(const char *input, char *output, int removelastsep);

#if defined PATH_MAX
#define MAXPATH PATH_MAX
#elif defined _MAX_PATH
#define MAXPATH _MAX_PATH
#else
#error
#endif

/* OS dependent functions */
double file_timestamp(const char *path);
double timestamp_now(void);
int oserror(lua_State *L, const char *msg);

int make_directory(lua_State *L, int idx);
int remove_directory(lua_State *L, int idx);
int find_first_program(lua_State *L, const char *path, char *full, int idx);
int glob_pattern(lua_State *L, int idx);

/* os.run(argv (1)[, workingdir (2)[, env(3)]]) */
int os_eval(lua_State *L);
/* os.cwd() */
int os_cwd(lua_State *L);
/* os.stat(path (1)) */
int os_stat(lua_State *L);


#ifdef _WIN32
#include <windows.h>
typedef HANDLE job_t;
#define JOB_NULL      NULL
#else
#include <sys/types.h>
#define JOB_NULL      -1
typedef pid_t job_t;
#endif

/* {run,shell}(argv (1)[, workingdir (2)]) */
job_t job_exec(lua_State *L, int echo);
job_t job_wait(job_t job, int *ret, int block);


/* The builder_t ties the DAG into the Lua VM */
#define BUILDER_METATYPE_NAME "BUILDER"

#define BUILDER_DEBUG_SETUP   0x00000001
#define BUILDER_DEBUG_ADD     0x00000002
#define BUILDER_DEBUG_BUILD   0x00000004
#define BUILDER_DUMP_DAG      0x00000008
#define BUILDER_RESET_DAG     0x00000010
#define BUILDER_ECHO_CMD      0x00000020
#define BUILDER_ECHO_TARGET   0x00000040
/* internal use only */
#define BUILDER_BUILDING      0x80000000

typedef struct node_t node_t;
typedef struct builder_t builder_t;

/* Creates a new builder_t object in both the Lua VM and the C environment */
builder_t *builder_alloc(lua_State *L, int jobs, unsigned int flags);
/* Build a given target from the C environment */
int builder_build(builder_t *builder, lua_State *L, const char *target);
/* Get the node with the given target */
node_t *builder_getnode(builder_t *builder, const char *target, int insert);
/* apply proxy nodes */
void builder_applyproxies(builder_t *builder);

/* Dump DAG stats */
void builder_stats(builder_t *builder, int memory, int histogram);
int builder_complete(builder_t *builder, const char *target);

/* template for recursively loading nested lists  */
#define FLATTEN_NESTED_LIST(action) \
   if(lua_istable(L, idx)) \
   { \
      int i, n = (int)lua_objlen(L, idx); \
      for(i = 1; i <= n; i++) \
      { \
         lua_rawgeti(L, idx, i); \
         if(action) \
            return 1; \
         lua_pop(L, 1); \
      } \
   } \
   else if(!lua_isnoneornil(L, idx))

#define copylstring(L, idx, len, dst) \
   do { const char *_str_ = lua_tolstring((L), (idx), &(len)); \
        memcpy((dst), _str_, (len) + 1); } while(0)

/* flag utils */
#define TEST_FLAG(obj, flag)  ((obj)->flags & (unsigned int)(flag))
#define SET_FLAG(obj, flag)   ((obj)->flags |= (unsigned int)(flag))
#define CLEAR_FLAG(obj, flag) ((obj)->flags &= ~(unsigned int)(flag))

#define SET_NODE_TAG(obj, tag) \
   (CLEAR_FLAG(obj, NODE_TAG_MASK), SET_FLAG(obj, \
               NODE_TAG_MASK & (unsigned int)(tag)))
#define GET_NODE_TAG(obj) TEST_FLAG(obj, NODE_TAG_MASK)


#define ARRAY_LENGTH(array) (sizeof(array) / sizeof(*array))


#endif /* __COMMON_H__ */
