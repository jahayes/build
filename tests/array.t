
#include <stdlib.h>

## function define_array(name, datatype)
##    local array = name .. '_t'

typedef struct <$array$> <$array$>;

<$array$> *<$name$>_alloc(size_t length);
void <$name$>_dealloc(<$array$> *array);

struct <$array$>
{
   size_t length;
   <$datatype$> data[1];
};

## end

## function implement_array(name, datatype)
##    local array = name .. '_t'

<$array$> *<$name$>_alloc(size_t length)
{
   size_t bytes = sizeof(<$array$>);
   if(length > 0)
      bytes += sizeof(<$datatype$>) * (length - 1);
   return (<$array$> *)calloc(1, bytes);
}

void <$name$>_dealloc(<$array$> *array)
{
   if(array != NULL)
      free(array);
}

## end
