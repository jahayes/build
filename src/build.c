/* see copyright notice in common.h */

#include "common.h"

#include "lualib.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#define PROGRAM    "build"
#define SCRIPTFILE "buildfile"
#define VERSION    "0.9.1"
#define COPYRIGHT  "Copyright (C) 2008-2012 John A. Hayes"
#define WEBSITE    "http://bitbucket.org/jahayes/build"

static void usage(int argc, const char *argv[])
{
   if(find_flag(argc, argv, 'v'))
   {
      message(PROGRAM" "VERSION" "COPYRIGHT"\n"
              LUA_RELEASE" "LUA_COPYRIGHT"\n"
              "\n"
              "  See "WEBSITE" for more information.\n");
      exit(EXIT_SUCCESS);
   }
   
   while(argc-- && (*argv == NULL || **argv != '-'))
      argv++;

   if(argc == -1)
      return;

   error(PROGRAM": illegal option -- %s\n", *argv + 1);
   error("usage: "PROGRAM" [-qervct] [-f FILE] [-j NUM] "
                          "[--KEY[=VALUE]]* [TARGET]*\n");
   error("   -q         Do not echo the target name to stdout.\n");
   error("   -e         Echo executed commands to stdout.\n");
   error("   -r         Reset DAG state after building each target.\n");
   error("   -v         Print version information and exit.\n");
   error("   -c         Run BUILDER:config() after loading the buildfile.\n");
   error("   -f FILE    Read FILE instead of './"SCRIPTFILE"'.\n");
   error("   -j NUM     Run up to NUM jobs at once.\n");
   error("  --KEY       Set KEY in the os.args table to true.\n");
   error("  --KEY=VALUE Set KEY in the os.args table to 'VALUE'.\n");
   error("    TARGET    Build TARGET.\n");
   error("\n");
   error("  Debugging options: [-SUABDMHC]\n");
   error("   -S         Log Lua setup.\n");
   error("   -U         Log undefined variable accesses.\n");
   error("   -A         Log adding targets.\n");
   error("   -B         Log building targets.\n");
   error("   -D         Dump DAG after building each target.\n");
   error("   -M         Dump DAG memory usage.\n");
   error("   -H         Dump DAG histograms.\n");
   error("   -C PREFIX  Complete the target name and exit.\n");
   error("\n");
   error("  See "WEBSITE" for more information.\n");
   exit(EXIT_FAILURE);
}

int main(int argc, const char *argv[], const char *env[])
{
   lua_State *L = NULL;
   builder_t *builder = NULL;

   int ret = EXIT_SUCCESS;
   unsigned int flags = 0;
   int jobs = atoi(find_arg(argc, argv, 'j', "1"));
   const char *buildfile = find_arg(argc, argv, 'f', SCRIPTFILE);
   int config = find_flag(argc, argv, 'c');
   int undefined = find_flag(argc, argv, 'U');
   int memory = find_flag(argc, argv, 'M');
   int histogram = find_flag(argc, argv, 'H');
   const char *complete = find_arg(argc, argv, 'C', NULL);

   if(!find_flag(argc, argv, 'q'))
      flags |= BUILDER_ECHO_TARGET;
   if(find_flag(argc, argv, 'e'))
      flags |= BUILDER_ECHO_CMD;
   if(find_flag(argc, argv, 'r'))
      flags |= BUILDER_RESET_DAG;

   if(find_flag(argc, argv, 'A'))
      flags |= BUILDER_DEBUG_ADD;
   if(find_flag(argc, argv, 'B'))
      flags |= BUILDER_DEBUG_BUILD;
   if(find_flag(argc, argv, 'D'))
      flags |= BUILDER_DUMP_DAG;
   if(find_flag(argc, argv, 'S'))
      flags |= BUILDER_DEBUG_SETUP;

   if(flags & BUILDER_DEBUG_SETUP)
      error("*S* creating lua state\n");
   
   L = luaL_newstate();
   luaL_openlibs(L);
   update_packagepath(L, flags & BUILDER_DEBUG_SETUP);
   append_table(L, flags & BUILDER_DEBUG_SETUP);
   append_string(L, flags & BUILDER_DEBUG_SETUP);
   append_os(L, flags & BUILDER_DEBUG_SETUP);
   open_builder(L, flags & BUILDER_DEBUG_SETUP);
   open_templates(L, flags & BUILDER_DEBUG_SETUP);
   load_args(L, argc, argv, flags & BUILDER_DEBUG_SETUP);
   load_env(L, env, flags & BUILDER_DEBUG_SETUP);

   if(undefined)
      warn_undefined_globals(L);

   argc = compact_args(argc, argv);
   usage(argc, argv);
   
   lua_getglobal(L, "debug");
   lua_getfield(L, 1, "traceback");           
   lua_remove(L, 1);
   
   /* stack:
    * 1) debug.traceback
    */

   if(flags & BUILDER_DEBUG_SETUP)
      error("*S* start loading '%s'\n", buildfile);

#define DIE(e, f) \
   do { if(e) { f ret = EXIT_FAILURE; goto cleanup; } } while(0)
#define DIE_MSG(e, a) \
   DIE(e, error(PROGRAM": "a" error:\n%s\n", lua_tostring(L, -1));)
   
   DIE_MSG(luaL_loadfile(L, buildfile), "loading");
   DIE_MSG(lua_pcall(L, 0, 0, 1), "running");
   
   if(flags & BUILDER_DEBUG_SETUP)
      error("*S* done loading '%s'\n", buildfile);

   /* reload debug.traceback just in case the script over writes it */
   lua_settop(L, 0);
   lua_getglobal(L, "debug");
   lua_getfield(L, 1, "traceback");           
   lua_remove(L, 1);

   builder = builder_alloc(L, jobs, flags);
   /* stack:
    * 1) debug.traceback
    * 2) builder
    */

#define HOOK(name) \
   DIE(call_hook(L, name, flags & BUILDER_DEBUG_SETUP), ;)

   if(config)
      HOOK("config");

   HOOK("setup");
   HOOK("load");

   builder_applyproxies(builder);

   if(complete != NULL)
      ret = builder_complete(builder, complete);
   else if(argc == 1)
      ret = builder_build(builder, L, "all");
   else
   {
      int i;
      for(i = 1; i < argc && ret == 0; i++)
         ret = builder_build(builder, L, argv[i]);
   }

   HOOK("save");

   if(memory || histogram)
      builder_stats(builder, memory, histogram);

cleanup:
   lua_settop(L, 0);
   builder = NULL;
   lua_close(L);
   return ret;
}
