/* see copyright notice in common.h */

#include "common.h"

/* table.imap(table (1), function (2)) */
static int table_imap(lua_State *L)
{
   int i, n;
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TFUNCTION);
   lua_settop(L, 2);
   lua_createtable(L, 0, 0);
   lua_pushvalue(L, 1);
   lua_remove(L, 1);
   n = (int)lua_objlen(L, 3);
   for(i = 1; i <= n; i++)
   {
      lua_pushvalue(L, 1);
      lua_rawgeti(L, 3, i);
      lua_call(L, 1, 1);
      lua_rawseti(L, 2, i);
   }
   lua_settop(L, 2);
   return 1;
}

/* table.map(table (1), function (2)) */
static int table_map(lua_State *L)
{
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TFUNCTION);
   lua_settop(L, 2);
   lua_createtable(L, 0, 0);
   lua_pushvalue(L, 1);
   lua_remove(L, 1);
   lua_pushnil(L);
   while(lua_next(L, 3))
   {
      lua_pushvalue(L, 1);
      lua_insert(L, 5);
      lua_call(L, 1, 1);
      lua_pushvalue(L, 4);
      lua_insert(L, 5);
      lua_settable(L, 2);
   }
   lua_settop(L, 2);
   return 1;
}

/* table.ifilter(table (1), function (2)) */
static int table_ifilter(lua_State *L)
{
   int i, n, j;
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TFUNCTION);
   lua_settop(L, 2);
   lua_createtable(L, 0, 0);
   lua_pushvalue(L, 1);
   lua_remove(L, 1);
   n = (int)lua_objlen(L, 3);
   for(i = 1, j = 1; i <= n; i++)
   {
      lua_rawgeti(L, 3, i);
      lua_pushvalue(L, 1);
      lua_pushvalue(L, 4);
      lua_call(L, 1, 1);
      luaL_checktype(L, 5, LUA_TBOOLEAN);
      if(lua_toboolean(L, 5))
      {
         lua_settop(L, 4);
         lua_rawseti(L, 2, j++);
      }
      else
         lua_settop(L, 3);
   }
   lua_settop(L, 2);
   return 1;
}

/* table.filter(table (1), function (2)) */
static int table_filter(lua_State *L)
{
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TFUNCTION);
   lua_settop(L, 2);
   lua_createtable(L, 0, 0);
   lua_pushvalue(L, 1);
   lua_remove(L, 1);
   lua_pushnil(L);
   while(lua_next(L, 3))
   {
      lua_pushvalue(L, 5);
      lua_pushvalue(L, 1);
      lua_insert(L, 6);
      lua_call(L, 1, 1);
      luaL_checktype(L, 6, LUA_TBOOLEAN);
      if(lua_toboolean(L, 6))
      {
         lua_settop(L, 5);
         lua_pushvalue(L, 4);
         lua_insert(L, 5);
         lua_settable(L, 2);
      }
      else
         lua_settop(L, 4);
   }
   lua_settop(L, 2);
   return 1;
}

static int copy_table(lua_State *L)
{
   lua_pushnil(L);
   while(lua_next(L, -2))
   {
      if(lua_istable(L, -1))
      {
         lua_createtable(L, 0, 0);
         lua_insert(L, -2);
         copy_table(L);
      }
      lua_pushvalue(L, -2);
      lua_insert(L, -2);
      lua_settable(L, -5);
   }
   lua_pop(L, 1);
   return 0;
}

/* table.copy(table (1)) */
static int table_copy(lua_State *L)
{
   luaL_checktype(L, 1, LUA_TTABLE);
   lua_settop(L, 1);
   lua_createtable(L, 0, 0);
   lua_insert(L, 1); /* output table (1), input table (2) */
   copy_table(L);
   return 1;
}

/* table.getfirstvalue(table (1), key (2)) */
static int table_getfirstvalue(lua_State *L)
{
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TSTRING);
   lua_settop(L, 2);
   lua_pushvalue(L, 1);
   lua_remove(L, 1);
   while(!lua_isnil(L, 2))
   {
      lua_pushvalue(L, 1);  /* 3 */
      lua_gettable(L, 2);
      if(!lua_isnil(L, 3))
         return 1;
      lua_settop(L, 2);
      lua_getfield(L, 2, "defaults");
      lua_remove(L, 2);
   }
   return 1;
}

static int flattentable(lua_State *L, int i)
{
   if(lua_istable(L, -1))
   {
      size_t j, len = lua_objlen(L, -1);
      for(j = 1; j <= len; j += 1)
      {
         lua_rawgeti(L, -1, (int)j);
         i = flattentable(L, i);
      }
      lua_pop(L, 1);
   }
   else
      lua_rawseti(L, 2, i++);
   return i;
}

/* table.getallvalues(table (1), key (2)) */
static int table_getallvalues(lua_State *L)
{
   int i = 1;
   luaL_checktype(L, 1, LUA_TTABLE);
   luaL_checktype(L, 2, LUA_TSTRING);
   lua_settop(L, 2);
   lua_createtable(L, 0, 0);  /* 2 */
   lua_pushvalue(L, 1);       /* 3 */
   lua_remove(L, 1);
   while(!lua_isnil(L, 3))
   {
      lua_pushvalue(L, 1);    /* 4 */
      lua_gettable(L, 3);
      if(!lua_isnil(L, 4))
         i = flattentable(L, i);
      else
         lua_settop(L, 3);
      lua_getfield(L, 3, "defaults");
      lua_remove(L, 3);
   }
   lua_settop(L, 2);
   return 1;
}

void append_table(lua_State *L, int debug)
{
   static const struct luaL_reg functions[] =
   {
      { "imap", table_imap },
      { "map", table_map },
      { "ifilter", table_ifilter },
      { "filter", table_filter },
      { "copy", table_copy },
      { "getfirstvalue", table_getfirstvalue },
      { "getallvalues", table_getallvalues },
      { NULL, NULL }
   };
   register_functions(L, LUA_TABLIBNAME, functions, debug);
}
