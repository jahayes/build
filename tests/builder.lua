
function BUILDER:config()
   BUILDER.test1 = "test1"
   print("BUILDER.test1", BUILDER.test1)
   print("self.test1", self.test1)

   self.test2 = "test2"
   print("BUILDER.test2", BUILDER.test2)
   print("self.test2", self.test2)

   self.test1 = "test3"
   print("BUILDER.test1", BUILDER.test1)
   print("self.test1", self.test1)

   BUILDER.test1 = "test4"
   print("BUILDER.test1", BUILDER.test1)
   print("self.test1", self.test1)

   self.test2 = "test5"
   print("BUILDER.test2", BUILDER.test2)
   print("self.test2", self.test2)
end

function BUILDER:load()
   local function print_target(self, args, output, deps)
      print(self, args, output, deps[1])
      return 0
   end

   local function add_cycle(list)
      local last = list[#list]
      for i = 1, #list do
         self:addgroup(list[i], { last }, print_target)
         last = list[i]
      end
   end

   add_cycle{"test1", "test2"}
   add_cycle{"test3", "test4", "test5"}
   add_cycle{"test6", "test7", "test8", "test9"}
   self:addgroup("all", {"test1", "test3", "test6"})
end




