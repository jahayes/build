function BUILDER:load() self:addgroup('all') end

print(os.cwd())
print(os.which('ls'))
assert(os.which('not a cmd') == nil, "found 'not a cmd'")
print(os.which('not a cmd', 'ls'))
print(os.eval({'ls', '-l'}, 'src'))

print('======================================')
for k,v in pairs(os.env) do
   print(k, v)
end
print('======================================')
print(os.run({'env'}))
print('======================================')
os.env.VAR1 = 'hi'
print(os.run({'env'}))
print('======================================')
print(os.run({'env'}, '.', {os.env, VAR2='bye'}))
print('======================================')
print(os.eval({'env'}, '.', {os.env, VAR2='bye'}))
print('======================================')
for _,filename in ipairs(os.glob('src/*.c')) do
   print(filename)
end
print('======================================')
for _,filename in ipairs(os.glob('src/*.c', 'lua/src/*.c')) do
   print(filename)
end
print('======================================')




