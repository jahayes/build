-- see copyright notice in buildbase/init.lua

function pathcat(...)
   return table.concat({...}, '/'):cleanup()
end

function makeoutputdir(output)
   os.mkdir(output:dirname())
end

function string.split(str, pattern, list)
   if string.find('', pattern, 1) then
      error('pattern matches empty string!')
   end
   
   local pos = 1
   list = list or {}
   local first, last = string.find(str, pattern, pos)
   while first do
      list[#list + 1] = string.sub(str, pos, first - 1)
      pos = last + 1
      first, last = string.find(str, pattern, pos)
   end
   list[#list + 1] = string.sub(str, pos)
   return list
end

function table.keys(t)
   local list = {}
   for key,_ in pairs(t) do
      list[#list + 1] = key
   end
   return list
end

function table.dump(t,b)
   b = b or ''
   for k,v in pairs(t) do
      if type(v) == 'table' then
         table.dump(v, b .. k .. '|')
      else
         print(b .. k, v)
      end
   end
end

local function stdouteval(cmd)
   local ret, stdout, stderr = os.eval(cmd)
   -- need () so we only take the first return value
   return (stdout:gsub('^%s*(.-)%s*$', '%1'))
end

if os.which('cmd.exe') then
   local out = stdouteval({'cmd.exe', '/c', 'ver'})
   _, _, os.name, os.release =
       out:find('Microsoft (%S+) %[Version (%d+%.%d+%.%d+)%]')
   os.cputype = os.env.Platform or 'i386'
   os.hostname = stdouteval('hostname')
elseif os.which('uname') then
   os.name = stdouteval({'uname', '-s'})
   os.release = stdouteval({'uname', '-r'})
   os.cputype = stdouteval({'uname', '-m'})
   os.hostname = stdouteval('hostname')
else
   error('cannot find OS info')
end

print(os.name, os.release, os.cputype, os.hostname)



