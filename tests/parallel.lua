
function BUILDER:load()
   local function sleep(self, args, output, deps)
      return self:run({'sleep', output})
   end

   local all = {}
   for i = 1,5 do
      local target = tostring(i)
      self:addgroup(target, nil, sleep)
      all[#all + 1] = target
   end
   self:addgroup('all', all)
end
