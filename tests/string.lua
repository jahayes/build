function BUILDER:load() self:addgroup("all") end

local function test_basename_dirname(str)
   print("test_basename_dirname(" .. str .. ")", str:dirname(), str:basename())
end

test_basename_dirname("../../dir1/dir2/../..")
test_basename_dirname("../../dir1/dir2/..//////..//////")

local function test_cleanup(str)
   print("test_cleanup(" .. str .. ")", str:cleanup())
end

test_cleanup("../../dir1/dir2/../..")
test_cleanup("../../dir1/dir2/..//////..//////")
test_cleanup("..//..//dir1//dir2//..//////..//////")
test_cleanup(".././.././dir1/./dir2/./..///.///..///.///")
test_cleanup("/./././dir1/./dir2/./../")
test_cleanup("./././dir1/./dir2/./../")

local str = '3c90f4a6'
for i = 1, 10 do
   local h = str:hash();
   local next = string.format('%08x', h)
   print(i, str, h, next, string.format('%d', h), string.format('%u', h))
   str = next
end

local str = '3c90f4a6'
for i = 1, 10 do
   local next = str:hash(true)
   print(i, str, next)
   str = next
end
