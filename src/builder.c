/* see copyright notice in common.h */

#include "common.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>


#define TREE_IS_NODE        (1 << 8)
/* interesting == set when a child node will be displayed by */
/* builder_complete() -- child node is phony or has an action */
#define TREE_IS_INTERESTING (TREE_IS_NODE << 1)

#define NODE_PHONY      (TREE_IS_INTERESTING << 1)
#define NODE_PROXY      (NODE_PHONY << 1)
#define NODE_DONE       (NODE_PROXY << 1)
#define NODE_ON_STACK   (NODE_DONE << 1)
#define NODE_IN_ORDER   (NODE_ON_STACK << 1)
#define NODE_CYCLE_HEAD (NODE_IN_ORDER << 1)
#define NODE_PRUNE      (NODE_CYCLE_HEAD << 1)
#define NODE_OLD        (NODE_PRUNE << 1)
#define NODE_NODEPS     (NODE_OLD << 1)

#define NODE_TAG_MASK   (TREE_IS_NODE - 1)

typedef unsigned short length_t;
typedef struct tree_t tree_t;
typedef struct proxy_t proxy_t;
typedef struct worker_t worker_t;

struct tree_t
{
   tree_t *prev;
   char *key;
   unsigned int flags;
   length_t length;
   
   length_t branches;
   tree_t **branch;
};

struct node_t
{
   tree_t *prev;
   char *key;
   unsigned int flags;
   length_t length;
   
   length_t dependencies;
   node_t **dependency;
   
   node_t *order;  /* proxy */
   double timestamp;
   
   int action;
   int args;
};

struct proxy_t
{
   proxy_t *next;
   char *root;
   node_t *proxy;
};

struct worker_t
{
   node_t *node;
   lua_State *L;
   job_t job;
   int ret;
   int ref;
};

struct builder_t
{
   unsigned int flags;
   /* action/args ref table */
   int map;
   /* table for lua functions and data */
   int table;
   tree_t dag;
   
   proxy_t *proxy;
   
   /* job pool */
   int jobs;
   int active;
   worker_t pool[1];
};



static builder_t *checkbuilder(lua_State *L, int idx)
{
   builder_t *builder = luaL_checkudata(L, idx, BUILDER_METATYPE_NAME);
   luaL_argcheck(L, builder != NULL, 1, BUILDER_METATYPE_NAME " expected");
   return builder;
}

static void checkarg(lua_State *L, int idx, int type)
{
   if(lua_type(L, idx) != type && !lua_isnoneornil(L, idx))
   {
      const char *msg = lua_pushfstring(L, "%s or nil expected, got %s",
                                        lua_typename(L, type),
                                        luaL_typename(L, idx));
      luaL_argerror(L, idx, msg);
   }
}

builder_t *builder_alloc(lua_State *L, int jobs, unsigned int flags)
{
   size_t bytes = sizeof(builder_t) + sizeof(worker_t) * ((size_t)jobs - 1);
   builder_t *builder = lua_newuserdata(L, bytes);
   memset(builder, 0, bytes);
   builder->flags = flags;
   builder->map = LUA_NOREF;
   builder->table = LUA_NOREF;
   builder->jobs = jobs;
   SET_FLAG(&builder->dag, TREE_IS_INTERESTING);
   
   if(TEST_FLAG(builder, BUILDER_DEBUG_SETUP))
      error("*S* creating new builder -- 0x%x\n", flags);
   
   luaL_getmetatable(L, BUILDER_METATYPE_NAME);
   lua_setmetatable(L, -2);

   lua_createtable(L, 0, 0);
   builder->map = lua_ref(L, 1);
   
   lua_createtable(L, 0, 0);
   builder->table = lua_ref(L, 1);
   
   return builder;
}

static void freedag(tree_t *tree, const char *key)
{
   if(tree->key != key)
      free(tree->key);

   if(TEST_FLAG(tree, TREE_IS_NODE))
   {
      node_t *node = (node_t *)tree;
      if(node->dependency != NULL)
         free(node->dependency);
   }
   else if(tree->branch != NULL)
   {
      length_t i;
      for(i = 0; i < tree->branches; i += 1)
      {
         freedag(tree->branch[i], tree->key + tree->length);
         free(tree->branch[i]);
      }
      free(tree->branch);
   }
}

static void freeproxy(proxy_t *proxy)
{
   while(proxy != NULL)
   {
      proxy_t *next = proxy->next;
      if(proxy->root != NULL)
         free(proxy->root);
      free(proxy);
      proxy = next;
   }
}

/* BUILDER:__gc(self (1)) */
static int builder__gc(lua_State *L)
{
   builder_t *builder = checkbuilder(L, 1);
   if(TEST_FLAG(builder, BUILDER_DEBUG_SETUP))
      error("*S* deleting builder\n");
   
   freedag(&builder->dag, NULL);
   freeproxy(builder->proxy);

   lua_getref(L, builder->map);
   lua_pushnil(L);  /* first key */
   while(lua_next(L, -2) != 0)
   {
      lua_unref(L, (int)lua_tointeger(L, -1));
      lua_pop(L, 1);
   }

   lua_unref(L, builder->map);
   lua_unref(L, builder->table);

   return 0;
}

/* BUILDER:__index(self (1), key (2)) */
static int builder__index(lua_State *L)
{
   builder_t *builder = checkbuilder(L, 1);
   lua_getref(L, builder->table);                  /* 3 */
   lua_pushvalue(L, 2);                            /* 4 */
   lua_rawget(L, 3);
   
   if(lua_isnoneornil(L, 4))
   {
      lua_pop(L, 2);
      luaL_getmetatable(L, BUILDER_METATYPE_NAME); /* 3 */
      lua_pushvalue(L, 2);                         /* 4 */
      lua_rawget(L, 3);
   }
   return 1;
}

/* BUILDER:__newindex(self (1), key (2), value (3)) */
static int builder__newindex(lua_State *L)
{
   builder_t *builder = checkbuilder(L, 1);
   lua_getref(L, builder->table);
   lua_replace(L, 1);
   lua_settable(L, 1);
   return 0;
}

static size_t lookup(tree_t *tree, char value)
{
   tree_t **branch = tree->branch;
   size_t min = 0, max = tree->branches;
   while(max - min > 1)
   {
      size_t mid = (min + max) / 2;
      if(value < *branch[mid]->key)
         max = mid;
      else
         min = mid;
   }
   return min;
}

node_t *builder_getnode(builder_t *builder, const char *name, int insert)
{
   char target[MAXPATH];
   tree_t **prev = NULL;
   tree_t *tree = &builder->dag;
   const char *tpos = normalize_path(name, target, 1);
   const char *tend = tpos + strlen(tpos);
   size_t idx = 0;

walk_tree:
   if(tree->branches > 0)
   {
      char p = *tpos;
      char k = *tree->branch[idx = lookup(tree, p)]->key;
      if(p == k)
      {
         char *kpos = (tree = *(prev = tree->branch + idx))->key;
         char *kend = kpos + tree->length;

         while(kpos < kend && tpos < tend && *tpos == *kpos)
         {
            kpos += 1;
            tpos += 1;
         }

         if(kpos == kend)
         {
            if(!TEST_FLAG(tree, TREE_IS_NODE))
               goto walk_tree;
            else if(tpos == tend)
               return (node_t *)tree;
         }
            
         if(insert)
         {
            tree_t *temp = calloc(1, sizeof(tree_t));
            temp->prev = tree->prev;
            temp->key = tree->key;
            temp->length = kpos - tree->key;
            if(TEST_FLAG(tree, TREE_IS_INTERESTING))
               SET_FLAG(temp, TREE_IS_INTERESTING);

            temp->branches = 1;
            temp->branch = malloc(sizeof(tree_t *));
            temp->branch[0] = tree;
            
            tree->key = kpos;
            tree->length = kend - kpos;
            tree->prev = temp;
            
            
            *prev = temp;
            tree = temp;
            idx = *tpos < *kpos ? 0 : 1;
         }
         else
            return NULL;
      }
      else if(p > k)
         idx += 1;
      else if(idx > 0)
         idx -= 1;
   }
   if(insert)
   {
      size_t i;
      node_t *node;
      tree_t **branch;
      if((branch = realloc(tree->branch,
                           sizeof(tree_t *) * (tree->branches + 1))) == NULL)
         return NULL;
      tree->branch = branch;
      
      if((node = calloc(1, sizeof(node_t))) == NULL)
         return NULL;

      tree->branches += 1;
      
      SET_FLAG(node, TREE_IS_NODE);
      node->length = tend - tpos;
      node->key = malloc((node->length + 1) * sizeof(char));
      memcpy(node->key, tpos, node->length * sizeof(char));
      node->key[node->length] = '\0';
      
      if(tree != &builder->dag)
         node->prev = tree;
      
      node->action = LUA_REFNIL;
      node->args = LUA_REFNIL;
      
      for(i = tree->branches - 1; i > idx; i -= 1)
         branch[i] = branch[i - 1];

      branch[idx] = (tree_t *)node;
      return node;
   }
   else
      return NULL;
}

static const char *node_name(const node_t *node, char *str)
{
   {
      const tree_t *pos = (const tree_t *)node;
      do
      {
         str += pos->length;
         pos = pos->prev;
      }
      while(pos != NULL);
   }
   str[0] = '\0';
   {
      const tree_t *pos = (const tree_t *)node;
      do
      {
         str -= pos->length;
         memcpy(str, pos->key, sizeof(char) * pos->length);
         pos = pos->prev;
      }
      while(pos != NULL);
   }
   return str;
}

static void statsdag(tree_t *tree,
                     size_t *count_tree, size_t *count_node,
                     size_t *hist_length,
                     size_t *hist_branches,
                     size_t *hist_dependencies)
{
   hist_length[tree->length] += 1;
   if(TEST_FLAG(tree, TREE_IS_NODE))
   {
      *count_node += 1;
      hist_dependencies[((node_t *)tree)->dependencies] += 1;
   }
   else
   {
      length_t i;
      *count_tree += 1;
      hist_branches[tree->branches] += 1;
      for(i = 0; i < tree->branches; i += 1)
      {
         statsdag(tree->branch[i], count_tree, count_node,
                  hist_length, hist_branches, hist_dependencies);
      }
   }
}

void builder_stats(builder_t *builder, int memory, int histogram)
{
   size_t count_tree = 0, count_node = 0;
   size_t hist_length[256], hist_branches[256], hist_dependencies[256];

   memset(hist_length, 0, sizeof(hist_length));
   memset(hist_branches, 0, sizeof(hist_branches));
   memset(hist_dependencies, 0, sizeof(hist_dependencies));

   statsdag(&builder->dag, &count_tree, &count_node,
            hist_length, hist_branches, hist_dependencies);

   {
      size_t i;
      size_t memory_text, memory_tree, memory_node;

      if(histogram)
         error("*H* hist of key lengths:\n");
      memory_text = sizeof(char) * count_node;
      for(i = 0; i < ARRAY_LENGTH(hist_length); i += 1)
      {
         size_t count = hist_length[i];
         memory_text += sizeof(char) * i * count;
         if(count != 0 && histogram)
            error("*H*    %3u %4u\n", i, count);
      }
      if(histogram)
         error("*H* hist of branches count:\n");
      memory_tree = sizeof(tree_t) * count_tree;
      for(i = 0; i < ARRAY_LENGTH(hist_branches); i += 1)
      {
         size_t count = hist_branches[i];
         memory_tree += sizeof(tree_t *) * i * count;
         if(count != 0 && histogram)
            error("*H*    %3u %4u\n", i, count);
      }
      if(histogram)
         error("*H* hist of dependencies count:\n");
      memory_node = sizeof(tree_t) * count_node;
      for(i = 0; i < ARRAY_LENGTH(hist_dependencies); i += 1)
      {
         size_t count = hist_dependencies[i];
         memory_node += sizeof(node_t *) * i * count;
         if(count != 0 && histogram)
            error("*H*    %3u %4u\n", i, count);
      }
      
      if(memory)
      {
         size_t total = memory_text + memory_tree + memory_node;
         error("*M* sizeof(tree_t) = %u\n", sizeof(tree_t));
         error("*M* sizeof(node_t) = %u\n", sizeof(node_t));
         error("*M* number of trees: %u\n", count_tree);
         error("*M* number of nodes: %u\n", count_node);
         error("*M* memory usage: %u %u %u bytes\n",
               memory_text, memory_tree, memory_node);
         error("*M* total memory usage: %u bytes (%.2f bytes per node)\n",
               total, (double)total / (double)count_node);
      }
   }
}

static int dump_target(tree_t *tree, char *target)
{
   if(TEST_FLAG(tree, TREE_IS_INTERESTING))
   {
      if(TEST_FLAG(tree, TREE_IS_NODE))
      {
         node_name((node_t *)tree, target);
         message("*C* %s\n", target);
      }
      else
      {
         length_t i;
         for(i = 0; i < tree->branches; i += 1)
         {
            tree_t *n = tree->branch[i];
            char *s = n->key + n->length;
            while(n->key < s && *s != '/')
               s -= 1;
            if(*s != '/')
               dump_target(n, target);
            else if(TEST_FLAG(n, TREE_IS_INTERESTING))
            {
               node_name((node_t *)n, target);
               target[strlen(target) - n->length + (s - n->key) + 1] = '\0';
               message("*C* %s\n", target);
            }
         }
      }
      return 0;
   }
   else
      return 1;
}

static tree_t *find_tree(builder_t *builder, const char *name, char *target)
{
   tree_t *tree = &builder->dag;
   const char *tpos = normalize_path(name, target, 0);
   const char *tend = tpos + strlen(tpos);
   
   if(tpos < tend)
   {
      const char *kpos, *kend;
      do
      {
         tree = tree->branch[lookup(tree, *tpos)];
         kpos = tree->key;
         kend = kpos + tree->length;
         
         while(tpos < tend && kpos < kend && *tpos == *kpos)
         {
            tpos += 1;
            kpos += 1;
         }
      }
      while(tpos < tend && kpos == kend);
   }
   
   return tpos == tend ? tree : NULL;
}

int builder_complete(builder_t *builder, const char *name)
{
   char target[MAXPATH];
   tree_t *tree = find_tree(builder, name, target);
   if(tree != NULL)
      return dump_target(tree, target);
   else
      return 1;
}

static void apply_proxy(builder_t *builder, tree_t *tree,
                        node_t *proxy, char *target)
{
   if(TEST_FLAG(tree, TREE_IS_NODE))
   {
      node_t *node = (node_t *)tree;
      node_name(node, target);

      if(node->action == LUA_REFNIL && node->args == LUA_REFNIL &&
         node->dependencies == 0 && node->dependency == NULL)
      {
         if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
            error("*A*    '%s'\n", target);

         SET_FLAG(node, NODE_PROXY);
         node->order = proxy;
      }
   }
   else
   {
      length_t i;
      for(i = 0; i < tree->branches; i += 1)
         apply_proxy(builder, tree->branch[i], proxy, target);
   }
}

void builder_applyproxies(builder_t *builder)
{
   char target[MAXPATH];
   proxy_t *proxy = builder->proxy;
   while(proxy != NULL)
   {
      tree_t *tree = find_tree(builder, proxy->root, target);
      if(tree != NULL)
      {
         if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
            error("*A* applying proxy '%s'\n", proxy->root);
         apply_proxy(builder, tree, proxy->proxy, target);
      }
      else
         error("bad proxy '%s'\n", proxy->root);
      proxy = proxy->next;
   }
}

static int getref(lua_State *L, builder_t *builder, int idx)
{
   int ref;
   if(lua_isnoneornil(L, idx))
      return LUA_REFNIL;
   
   lua_getref(L, builder->map);
   lua_pushvalue(L, idx);
   lua_gettable(L, -2);
   if(lua_isnil(L, -1))
   {
      lua_pop(L, 1);
      lua_pushvalue(L, idx);
      ref = lua_ref(L, 1);
      lua_pushvalue(L, idx);
      lua_pushinteger(L, ref);
      lua_settable(L, -3);
      lua_pop(L, 1);
   }
   else
   {
      ref = (int)lua_tointeger(L, -1);
      lua_pop(L, 2);
   }
   return ref;
}

static int appendchildren(builder_t *builder, node_t *node,
                          const char *name, int prune)
{
   node_t *child;
   if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
      error("*A*      '%s'\n", name);
   
   if((child = builder_getnode(builder, name, 1)) == NULL)
      return 1;
   /* a target should not have itself as a dependency */
   if(child != node)
   {
      if(!TEST_FLAG(child, prune))
      {
         node_t **dependency = node->dependency;
         length_t idx = node->dependencies;

         if((dependency = realloc(dependency,
                                  sizeof(node_t *) * (idx + 1))) == NULL)
            return 1;
         node->dependency = dependency;
         
         node->dependencies = idx + 1;
         dependency[idx] = child;
         SET_FLAG(child, prune);
      }
   }
   else
      error("%s depends on itself\n", name);
   return 0;
}

static int addchildren(lua_State *L, builder_t *builder, node_t *node,
                       int prune, int idx)
{
   FLATTEN_NESTED_LIST(addchildren(L, builder, node, prune, -1))
   {
      const char *name = lua_tostring(L, idx);
      if(appendchildren(builder, node, name, prune))
         return 1;
   }
   return 0;
}

/* BUILDER:add[file,group](self (1), target (2), dependencies (3),
                           action (4), args (5), tag (6)) */
static int addnode(lua_State *L, int append, int phony)
{
   node_t *node;
   int interesting = phony;
   builder_t *builder = checkbuilder(L, 1);
   const char *name = luaL_checkstring(L, 2);
   int tag = luaL_optint(L, 6, 0);
   checkarg(L, 3, LUA_TTABLE);
   checkarg(L, 4, LUA_TFUNCTION);
   checkarg(L, 5, LUA_TTABLE);

   if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
      error("*A* adding '%s' append = %d phony = %d\n", name, append, phony);

   if((node = builder_getnode(builder, name, 1)) == NULL)
      return oserror(L, "getnode()");

   append = append || TEST_FLAG(node, NODE_OLD);
   if(node->dependencies > 0 && !append)
      return luaL_error(L, "can not append to '%s'", name);

   if(tag < 0)
   {
      if(TEST_FLAG(node, NODE_OLD))
      {
         free(node->dependency);
         node->dependency = NULL;
         node->dependencies = 0;
      }
      SET_FLAG(node, NODE_NODEPS);
      tag = -tag;
   }
   else
      CLEAR_FLAG(node, NODE_NODEPS);

   SET_NODE_TAG(node, tag);
   CLEAR_FLAG(node, NODE_OLD);
   if(phony)
      SET_FLAG(node, NODE_PHONY);

   if(!lua_isnoneornil(L, 3))
   {
      if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
         error("*A*   dependencies:\n");

      if(append && node->dependency != NULL)
      {
         length_t i;
         node_t **dependency = node->dependency;
         for(i = 0; i < node->dependencies; i += 1)
            SET_FLAG(dependency[i], NODE_PRUNE);
      }
      
      if(addchildren(L, builder, node, (append ? NODE_PRUNE : 0), 3))
         return oserror(L, "adddependencies()");

      if(append && node->dependency != NULL)
      {
         length_t i;
         node_t **dependency = node->dependency;
         for(i = 0; i < node->dependencies; i += 1)
            CLEAR_FLAG(dependency[i], NODE_PRUNE);
      }
   }

   if(lua_isfunction(L, 4))
   {
      node->action = getref(L, builder, 4);
      node->args = getref(L, builder, 5);
      if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
         error("*A*   action: args = (%d, %d)\n", node->action, node->args);
      interesting = 1;
   }
   
   if(interesting)
   {
      tree_t *pos = (tree_t *)node;
      while(pos != NULL && !TEST_FLAG(pos, TREE_IS_INTERESTING))
      {
         SET_FLAG(pos, TREE_IS_INTERESTING);
         pos = pos->prev;
      }
   }
   
   return 0;
}

static int builder_addfile(lua_State *L)
{
   return addnode(L, 0, 0);
}

static int builder_addgroup(lua_State *L)
{
   return addnode(L, 1, 1);
}

/* BUILDER:addproxy(self (1), root (2), proxy (3)) */
static int builder_addtree(lua_State *L)
{
   proxy_t *frame;
   builder_t *builder = checkbuilder(L, 1);
   const char *root = luaL_checkstring(L, 2);
   const char *proxy = luaL_checkstring(L, 3);

   if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
      error("*A* adding proxy '%s' to '%s'\n", proxy, root);

   if((frame = malloc(sizeof(proxy_t))) == NULL)
      return oserror(L, "malloc()");
   
   frame->next = builder->proxy;
   builder->proxy = frame;
   
   if((frame->root = strdup(root)) == NULL)
      return oserror(L, "stdup()");

   if((frame->proxy = builder_getnode(builder, proxy, 1)) == NULL)
      return oserror(L, "getnode()");

   return 0;
}

/* BUILDER:updatedeps(self (1), target (2), path (3), name (4)) */
static int builder_updatedeps(lua_State *L)
{
   builder_t *builder = checkbuilder(L, 1);
   const char *fullname = luaL_checkstring(L, 2);
   const char *path = luaL_checkstring(L, 3);
   const char *name = luaL_optstring(L, 4, fullname);
   node_t *node = builder_getnode(builder, fullname, 1);

   unsigned int count = 0;
   FILE *file = fopen(path, "rt");
   if(file != NULL)
   {
      char line[1024];
#define isescapednewline(str) ((str)[0] == '\\' && (str)[1] == '\n')
#define ismultiline() (len > 1 && isescapednewline(line + len - 2))
#define myisspace(str) (isspace(*(str)) || isescapednewline(str))
      
      while(fgets(line, sizeof(line), file) != NULL)
      {
         size_t len = strlen(line);
         char *lp = strstr(line, name);
         if(lp == NULL)
         {
            if(strchr(line, ':') != NULL)
            {
               while(ismultiline() && fgets(line, sizeof(line), file) != NULL)
                  len = strlen(line);
            }
         }
         else
         {
            lp = strchr(lp, ':');
            while(lp == NULL && ismultiline())
            {
               if(fgets(line, sizeof(line), file) == NULL)
                  break;
               len = strlen(line);
               lp = strchr(line, ':');
            }
            lp += 1;
            do
            {
               do
               {
                  while(isspace(*lp))
                     lp += 1;
                  
                  if(isescapednewline(lp))
                  {
                     if(fgets(line, sizeof(line), file) == NULL)
                        break;
                     lp = line;
                  }
               }
               while(myisspace(lp));
               
               if(*lp != '\0')
               {
                  char deps[MAXPATH];
                  char *dp = deps;
                  while(*lp != '\0' && !myisspace(lp))
                  {
                     if(*lp == '\\')
                        lp += 1;
                     
                     *dp++ = *lp++;
                  }
                  *dp = '\0';
                  if(count == 0 && node->dependency != NULL)
                  {
                     free(node->dependency);
                     node->dependency = NULL;
                     node->dependencies = 0;
                  }
                  if(appendchildren(builder, node, deps, NODE_PRUNE))
                     return oserror(L, "adddependency()");
                  count += 1;
               }
            }
            while(myisspace(lp));
         }
      }
      fclose(file);

      if(node->dependency != NULL)
      {
         length_t i;
         node_t **dependency = node->dependency;
         for(i = 0; i < node->dependencies; i += 1)
            CLEAR_FLAG(dependency[i], NODE_PRUNE);
      }   
   }
   return 0;
}

#define MAGIC sizeof(short)

static const char *writelength(size_t len, FILE *file)
{
   unsigned short value = (unsigned short)len;
   return fwrite(&value, sizeof(short), 1, file) != 1 ? "fwrite()" : NULL;
}

static const char *readlength(size_t *len, FILE *file)
{
   unsigned short value;
   if(fread(&value, sizeof(short), 1, file) != 1)
      return "fread()";
   *len = value;
   return NULL;
}

static const char *writestring(const char *str, FILE *file)
{
   const char *msg;
   size_t len = strlen(str);
   if((msg = writelength(len, file)) != NULL)
      return msg;
   return fwrite(str, sizeof(char), len, file) != len ? "fwrite()" : NULL;
}

static const char *readnode(builder_t *builder, FILE *file, node_t **node)
{
   const char *msg;
   size_t len;
   char target[MAXPATH];

   if((msg = readlength(&len, file)) != NULL)
      return msg;
   if(fread(target, sizeof(char), len, file) != len)
      return "fread()";
   target[len] = '\0';

   if((*node = builder_getnode(builder, target, 1)) == NULL)
      return "getnode()";
   return NULL;
}

static const char *savenode(node_t *node, FILE *file)
{
   const char *msg = NULL;
   char target[MAXPATH];
   node_t **dependency = node->dependency;

   node_name(node, target);
   if((msg = writestring(target, file)) != NULL)
      return msg;

   if(TEST_FLAG(node, NODE_NODEPS) || dependency == NULL)
   {
      if((msg = writelength(0, file)) != NULL)
         return msg;
   }
   else
   {
      length_t i;
      if((msg = writelength(node->dependencies, file)) != NULL)
         return msg;
      for(i = 0; i < node->dependencies; i += 1)
      {
         node_name(dependency[i], target);
         if((msg = writestring(target, file)) != NULL)
            return msg;
      }
   }
   return NULL;
}

static const char *loadnode(builder_t *builder, FILE *file, node_t **node)
{
   const char *msg;
   size_t length;

   if((msg = readnode(builder, file, node)) != NULL)
      return msg;

   if((msg = readlength(&length, file)) != NULL)
      return msg;
   
   if(length > 0)
   {
      size_t i;
      node_t **dependency;
      if((dependency = malloc(sizeof(node_t *) * length)) == NULL)
         return "malloc()";
      (*node)->dependency = dependency;
      (*node)->dependencies = 0;
      for(i = 0; i < length; i += 1)
      {
         if((msg = readnode(builder, file, dependency + i)) != NULL)
            return msg;
      }
      (*node)->dependencies = length;
   }
   return NULL;
}

/* BUILDER:loadgraph(self (1), paths (2)) */
static int builder_loadgraph(lua_State *L)
{
   int i, n, loaded = 0;
   builder_t *builder = checkbuilder(L, 1);
   luaL_checktype(L, 2, LUA_TTABLE);
   lua_settop(L, 2);

   n = (int)lua_objlen(L, 2);
   for(i = 1; i <= n; i += 1)
   {
      FILE *file;
      const char *path;
      lua_rawgeti(L, 2, i);
      path = luaL_checkstring(L, 3);
      if((file = fopen(path, "rt")) != NULL)
      {
         size_t j, length;
         const char *msg;

         if((msg = readlength(&length, file)) != NULL)
            goto cleanup;
         if(length != MAGIC)
         {
            msg = "bad magic";
            goto cleanup;
         }

         if((msg = readlength(&length, file)) != NULL)
            goto cleanup;
         for(j = 0; j < length; j += 1)
         {
            node_t *node;
            if((msg = loadnode(builder, file, &node)) != NULL)
               goto cleanup;

            SET_NODE_TAG(node, i);
            SET_FLAG(node, NODE_OLD);
         }
cleanup:
         if(fclose(file))
            msg = "fclose()";
         if(msg != NULL)
            return oserror(L, msg);
         loaded += 1;
      }
      lua_settop(L, 2);
   }
   lua_pushboolean(L, loaded == n);
   return 1;
}


struct save_state
{
   FILE *file;
   int count;
};

static const char *savedag(lua_State *L, builder_t *builder, tree_t *tree,
                           struct save_state *state, size_t n)
{
   const char *msg = NULL;
   if(TEST_FLAG(tree, TREE_IS_NODE))
   {
      node_t *node = (node_t *)tree;
      size_t tag = GET_NODE_TAG(node);
      if(TEST_FLAG(node, NODE_OLD))
      {
         char target[MAXPATH];
         node_name(node, target);

         if(TEST_FLAG(builder, BUILDER_DEBUG_ADD))
            error("*A* '%s' is old\n", target);
         
         if(lua_isfunction(L, 2))
         {
            lua_pushvalue(L, 2);
            lua_pushvalue(L, 1);
            lua_pushstring(L, target);
            lua_pushinteger(L, tag);
            if(lua_pcall(L, 3, 0, 3))
            {
               /* don't bail out because we don't want to corrupt the files */
               error("error handling '%s': %s\n", target, lua_tostring(L, -1));
            }
         }
      }
      else if(1 <= tag && tag <= n && !TEST_FLAG(node, NODE_PHONY))
      {
         if((msg = savenode(node, state[tag].file)) != NULL)
            goto cleanup;
         state[tag].count += 1;
      }
   }
   else
   {
      length_t i;
      for(i = 0; i < tree->branches && msg == NULL; i += 1)
         msg = savedag(L, builder, tree->branch[i], state, n);
   }

cleanup:
   return msg;
}

/* BUILDER:savegraph(self (1), paths (2), oldhandler (3)) */
static int builder_savegraph(lua_State *L)
{
   size_t i, n;
   struct save_state *state;
   const char *msg = NULL;
   builder_t *builder = checkbuilder(L, 1);
   luaL_checktype(L, 2, LUA_TTABLE);
   checkarg(L, 3, LUA_TFUNCTION);

   if((state = calloc(n = lua_objlen(L, 2), sizeof(*state))) == NULL)
      return oserror(L, "calloc()");
   state -= 1;
   
   for(i = 1; i <= n; i += 1)
   {
      const char *path;
      lua_rawgeti(L, 2, (int)i);
      path = luaL_checkstring(L, -1);
      if((state[i].file = fopen(path, "wt")) == NULL)
      {
         msg = "fopen()";
         goto cleanup;
      }
      
      if((msg = writelength(MAGIC, state[i].file)) != NULL)
         goto cleanup;
      if((msg = writelength(state[i].count, state[i].file)) != NULL)
         goto cleanup;

      lua_pop(L, 1);
   }

   lua_remove(L, 2);
   lua_settop(L, 2);
   lua_getglobal(L, "debug");
   lua_getfield(L, 3, "traceback");
   lua_remove(L, 3);

   savedag(L, builder, &builder->dag, state, n);

   /* rewrite the header */
   for(i = 1; i <= n; i += 1)
   {
      if(fseek(state[i].file, 0, SEEK_SET))
      {
         msg = "fseek()";
         goto cleanup;
      }
      
      if((msg = writelength(MAGIC, state[i].file)) != NULL)
         goto cleanup;
      if((msg = writelength(state[i].count, state[i].file)) != NULL)
         goto cleanup;
   }
cleanup:
   for(i = 1; i <= n; i += 1)
   {
      if(state[i].file != NULL)
      {
         if(fclose(state[i].file))
            msg = "fclose()";
      }
   }
   state += 1;
   free(state);
   if(msg != NULL)
      return oserror(L, msg);
   return 0;
}

/* BUILDER:run(self (1), argv (2)[, workingdir (3)[, env(4)]]) */
static int builder_run(lua_State *L)
{
   job_t job;
   builder_t *builder = checkbuilder(L, 1);
   if(!TEST_FLAG(builder, BUILDER_BUILDING))
      return luaL_error(L, "only call BUILDER:run() when building");
   lua_remove(L, 1);

   if((job = job_exec(L, TEST_FLAG(builder, BUILDER_ECHO_CMD))) == JOB_NULL)
      return oserror(L, "job_exec()");
   
   if(builder->jobs == 1)
   {
      int ret;
      if(job_wait(job, &ret, 1) != job)
         return oserror(L, "job_wait()");
      
      lua_pushinteger(L, ret);
      return 1;
   }
   else
   {
      worker_t *worker = builder->pool;
      worker_t *stop = worker + builder->active;
      while(worker < stop && worker->L != L)
         worker += 1;
      worker->job = job;
      return lua_yield(L, 0);
   }
}

static node_t *reversetopsort(node_t *node, node_t *next)
{
   length_t i;
   SET_FLAG(node, NODE_ON_STACK);
   for(i = 0; i < node->dependencies; i += 1)
   {
      node_t *temp = node->dependency[i];
      while(TEST_FLAG(temp, NODE_PROXY))
         temp = temp->order;
      /* test for ON_STACK to stop cycles */
      /* and mark the head of the cycle so parallel builds work */
      if(TEST_FLAG(temp, NODE_ON_STACK))
      {
         char target[MAXPATH];
         SET_FLAG(temp, NODE_CYCLE_HEAD);
         error("cycle found: %s ==>", node_name(temp, target));
         while(temp != NULL && temp != node)
         {
            length_t j = 0;
            error(" %s ==>", node_name(temp, target));
            while(j < temp->dependencies && 
                  !TEST_FLAG(temp->dependency[j], NODE_ON_STACK))
               j += 1;
            temp = temp->dependency[j];
         }
         error(" %s\n", node_name(node, target));
      }
      else if(!TEST_FLAG(temp, NODE_IN_ORDER | NODE_DONE))
         next = reversetopsort(temp, next);
   }
   
   CLEAR_FLAG(node, NODE_ON_STACK);
   SET_FLAG(node, NODE_IN_ORDER);
   node->order = next;
   return node;
}

static node_t *topsort(node_t *node)
{
   node_t *head = NULL;
   while(TEST_FLAG(node, NODE_PROXY))
      node = node->order;
   if(!TEST_FLAG(node, NODE_DONE))
   {
      node_t *tail = reversetopsort(node, NULL);
      while(tail != NULL)
      {
         node_t *next = tail->order;
         /* clear the flags now so if build() fails its in a clean state */
         CLEAR_FLAG(tail, NODE_IN_ORDER | NODE_ON_STACK);
         
         /* remove the leaf nodes -- but leave missing nodes */
         if(tail->dependencies == 0 && tail->action == LUA_REFNIL)
         {
            char target[MAXPATH];
            if(TEST_FLAG(tail, NODE_PHONY))
               tail->timestamp = timestamp_now();
            else
               tail->timestamp = file_timestamp(node_name(tail, target));
            if(tail->timestamp > 0.0)
            {
               SET_FLAG(tail, NODE_DONE);
               tail->order = NULL;
            }
         }
         if(!TEST_FLAG(tail, NODE_DONE))
         {
            tail->order = head;
            head = tail;
         }
         tail = next;
      }
   }
   return head;
}

static int updatetimestamp(node_t *node)
{
   if(!TEST_FLAG(node, NODE_PHONY))
   {
      char target[MAXPATH];
      node_name(node, target);
      if((node->timestamp = file_timestamp(target)) != 0)
      {
         if(TEST_FLAG(node, NODE_PROXY) &&
            node->timestamp < node->order->timestamp)
            node->timestamp = node->order->timestamp;
         return 1;
      }
      
      /* make missing leafs force a re-build */
   }
   node->timestamp = timestamp_now();
   return 0;
}

static int checkdependencies(node_t *node, double *timestamp)
{
   length_t i;
   double ts = 0;
   for(i = 0; i < node->dependencies; i += 1)
   {
      node_t *temp = node->dependency[i];
      if(TEST_FLAG(temp, NODE_PROXY) && !TEST_FLAG(temp, NODE_DONE))
      {
         updatetimestamp(temp);
         temp->flags |= temp->order->flags & NODE_DONE;
      }
      /* the head node of a cycle is always "done" */
      /* (this makes parallel builds work) */
      if(!TEST_FLAG(temp, NODE_DONE | NODE_CYCLE_HEAD))
         return 1;
      if(temp->timestamp > ts)
         ts = temp->timestamp;
   }
   *timestamp = ts;
   return 0;
}

static int isdirty(node_t *node, double timestamp)
{
   int dirty = 0;
   /* leafs are never dirty (even when missing) */ 
   if(updatetimestamp(node))
      dirty = node->timestamp < timestamp;
   else
      dirty = node->action != LUA_REFNIL;
   if(!dirty)
      SET_FLAG(node, NODE_DONE);
   return dirty;
}

static void dumpnode(node_t *node, const char *prefix, const char *action)
{
   static const char *flag_names[] = {
      "PHONY", "DONE", "ON_STACK", "IN_ORDER",
      "CYCLE_HEAD", "PRUNE", "OLD", "NODEPS",
   };
   
   char target[MAXPATH];
   error("%s %s '%s'\n", prefix, action, node_name(node, target));
   error("%s    timestamp:    %.2f\n", prefix, node->timestamp);
   error("%s    action:       %d, %d\n", prefix, node->action, node->args);
   error("%s    tag:          %d\n", prefix, GET_NODE_TAG(node));
   {
      size_t i;
      error("%s    flags:       ", prefix);
      for(i = 0; i < ARRAY_LENGTH(flag_names); i += 1)
      {
         if(TEST_FLAG(node, (NODE_PHONY << i)))
            error(" %s", flag_names[i]);
      }
      error("\n");
   }
   error("%s    dependencies: %d\n", prefix, node->dependencies);
   {
      length_t i;
      for(i = 0; i < node->dependencies; i += 1)
      {
         node_t *dep = node->dependency[i];
         error("%s       '%s' (%.2f) %s\n", prefix,
               node_name(dep, target), dep->timestamp,
               TEST_FLAG(dep, NODE_DONE) ? "clean" : "dirty");
      }
   }
}

static void dumpdag(tree_t *tree, const char *prefix, const char *action)
{
   if(TEST_FLAG(tree, TREE_IS_NODE))
      dumpnode((node_t *)tree, prefix, action);
   else
   {
      length_t i;
      for(i = 0; i < tree->branches; i += 1)
         dumpdag(tree->branch[i], prefix, action);
   }
}

static void resetdag(tree_t *tree)
{
   if(TEST_FLAG(tree, TREE_IS_NODE))
      CLEAR_FLAG(tree, NODE_ON_STACK | NODE_CYCLE_HEAD |
                 NODE_IN_ORDER | NODE_DONE);
   else
   {
      length_t i;
      for(i = 0; i < tree->branches; i += 1)
         resetdag(tree->branch[i]);
   }
}

static void pushaction(lua_State *L, builder_t *builder, node_t *node)
{
   /* stack:
    * 1) debug.traceback
    * 2) builder
    */
   char target[MAXPATH];
   node_name(node, target);
   if(TEST_FLAG(builder, BUILDER_ECHO_TARGET))
      message("%s\n", target); 
   if(TEST_FLAG(builder, BUILDER_DEBUG_BUILD))
      dumpnode(node, "*B*", "updating:");
   
   lua_settop(L, 2);
   lua_getref(L, node->action);
   lua_pushvalue(L, 2);
   lua_getref(L, node->args);
   lua_pushstring(L, target);
   
   if(node->dependencies > 0)
   {
      length_t i;
      lua_createtable(L, node->dependencies, 0);
      for(i = 0; i < node->dependencies; i += 1)
      {
         node_name(node->dependency[i], target);
         lua_pushstring(L, target);
         lua_rawseti(L, -2, i + 1);
      }
   }
   else
      lua_pushnil(L);
}

static int updatenode(builder_t *builder, node_t *node, int ret,
                      const char **msg)
{
   int missing = 0;
   char target[MAXPATH];
   node_name(node, target);
   if(TEST_FLAG(builder, BUILDER_DEBUG_BUILD))
      error("*B*    updated '%s' [%d]\n", target, ret);
   node->timestamp = file_timestamp(target);
   
   if(node->timestamp <= 0.0 || TEST_FLAG(node, NODE_PHONY))
   {
      if((missing = !TEST_FLAG(node, NODE_PHONY)))
         *msg = "missing";
      node->timestamp = timestamp_now();
   }
   
   if(ret != 0 || missing)
   {
      CLEAR_FLAG(node, NODE_DONE);
      return 1;
   }
   else
   {
      SET_FLAG(node, NODE_DONE);
      return 0;
   }
}

static int resumeworker(lua_State *L, builder_t *builder, worker_t *worker,
                        int narg, const char **msg)
{
   int err = lua_resume(worker->L, narg);
   if(err == 0)
   {
      worker_t *last = builder->pool + (builder->active -= 1);
      if(worker != last)
      {
         worker_t temp = *worker;
         *worker = *last;
         *last = temp;
      }
      
      last->ret = luaL_checkint(last->L, -1);
      lua_unref(L, last->ref);
      last->ref = LUA_NOREF;
      last->L = NULL;
      return updatenode(builder, last->node, last->ret, msg);
   }
   else if(err != LUA_YIELD)
   {
      worker->ret = err;
      *msg = lua_tostring(worker->L, -1);
      return 1;
   }
   else
      return 0;
}

static int popworkers(lua_State *L, builder_t *builder, const char **msg,
                      int block)
{
   while(builder->active > 0)
   {
      int ret = 0;
      job_t job = job_wait(JOB_NULL, &ret, block);
      if(job != JOB_NULL)
      {
         worker_t *worker = builder->pool;
         worker_t *stop = worker + builder->active;
         while(worker < stop && worker->job != job)
            worker += 1;
         
         worker->ret = ret;
         lua_pushinteger(worker->L, ret);
         if(resumeworker(L, builder, worker, 1, msg))
            return 1;
      }
      else
         return block ? oserror(L, "job_wait()") : 0;
      
      block = builder->active == builder->jobs;
   }
   return 0;
}

int builder_build(builder_t *builder, lua_State *L, const char *name)
{
   /* stack:
    * 1) debug.traceback
    * 2) builder
    */
   int ret = 0;
   char target[MAXPATH];
   
   const char *msg = NULL;
   {
      node_t *node = builder_getnode(builder, name, 0);
      
      SET_FLAG(builder, BUILDER_BUILDING);
      if(TEST_FLAG(builder, BUILDER_DEBUG_BUILD))
         error("*B* start building '%s'\n", name);
      
      if(node == NULL)
         msg = "unknown target";
      else
      {
         node_name(node, target);
         node = topsort(node);
      }
      if(builder->jobs == 1)
      {
         while(node != NULL && ret == 0 && msg == NULL)
         {
            double timestamp = 0;
            
            if(checkdependencies(node, &timestamp))
            {
               msg = "children dirty";
               break;
            }
            
            if(isdirty(node, timestamp))
            {
               if(node->action == LUA_REFNIL)
               {
                  msg = "no action";
                  break;
               }
               pushaction(L, builder, node);
               if(lua_pcall(L, 4, 1, 1))
               {
                  msg = lua_tostring(L, -1);
                  break;
               }
               ret = luaL_checkint(L, 3);
               if(updatenode(builder, node, ret, &msg))
                  break;
            }
            node = node->order;
         }
      }
      else
      {
         while(node != NULL && ret == 0 && msg == NULL)
         {
            /* poll the list for the first ready node */
            /* in the worse case it will look at the first job + 1 jobs */
            node_t **pos = &node;
            while(*pos != NULL)
            {
               double timestamp = 0;
               if(!checkdependencies(*pos, &timestamp))
               {
                  if(isdirty(*pos, timestamp))
                  {
                     worker_t *worker;
                     if((*pos)->action == LUA_REFNIL)
                     {
                        msg = "no action";
                        break;
                     }
                     
                     if(popworkers(L, builder, &msg,
                                   builder->active == builder->jobs))
                     {
                        node = builder->pool[builder->active].node;
                        ret = builder->pool[builder->active].ret;
                        break;
                     }
                     
                     worker = builder->pool + builder->active;
                     builder->active += 1;
                     
                     worker->L = lua_newthread(L);
                     /* get a ref to the thread and pop it */
                     /* off the main stack */
                     worker->ref = lua_ref(L, 1);
                     worker->job = 0;
                     worker->node = *pos;
                     worker->ret = 0;
                     
                     lua_xcopy(L, worker->L, 2);
                     pushaction(worker->L, builder, *pos);
                     if(resumeworker(L, builder, worker, 4, &msg))
                     {
                        node = worker->node;
                        ret = worker->ret;
                        break;
                     }
                  }
                  
                  *pos = (*pos)->order;
                  break;
               }
               pos = &(*pos)->order;
            }
            
            if(*pos == NULL && popworkers(L, builder, &msg, 1))
            {
               node = builder->pool[builder->active].node;
               ret = builder->pool[builder->active].ret;
               break;
            }
         }
         
         while(builder->active > 0)
         {
            int ret2;
            if(job_wait(builder->pool->job, &ret2, 1) == builder->pool->job)
            {
               lua_pushinteger(builder->pool->L, ret2);
               if(resumeworker(L, builder, builder->pool, 1, &msg))
               {
                  node = builder->pool[builder->active].node;
                  ret = builder->pool[builder->active].ret;
               }
            }
         }
      }
      
      if(node != NULL)
         node_name(node, target);
   }
   
   if(TEST_FLAG(builder, BUILDER_DUMP_DAG))
      dumpdag(&builder->dag, "*D*", "target:");
   
   if(TEST_FLAG(builder, BUILDER_RESET_DAG))
   {
      if(TEST_FLAG(builder, BUILDER_DEBUG_BUILD))
         error("*B* reseting builder's DAG\n");
      resetdag(&builder->dag);
   }
   
   if(ret != 0)
      error("error building: '%s': exit %d\n", name, ret);
   else if(msg != NULL)
      error("error building: '%s': %s\n", name, msg);
   else if(TEST_FLAG(builder, BUILDER_DEBUG_BUILD))
      error("*B* done building '%s'\n", name);
   
   CLEAR_FLAG(builder, BUILDER_BUILDING);
   return ret != 0 || msg != NULL;
}

void open_builder(lua_State *L, int debug)
{
   static const struct luaL_reg methods[] =
   {
      { "__gc", builder__gc },
      { "__index", builder__index },
      { "__newindex", builder__newindex },
      
      { "addfile", builder_addfile },
      { "addgroup", builder_addgroup },
      { "addtree", builder_addtree },
      { "updatedeps", builder_updatedeps },
      { "loadgraph", builder_loadgraph },
      { "savegraph", builder_savegraph },
      
      { "run", builder_run },
      
      { NULL, NULL }
   };
   register_methods(L, BUILDER_METATYPE_NAME, methods, debug);
}
