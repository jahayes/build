require('buildbase')

local mode = os.args.mode or 'Release'
local platform_dir = pathcat(os.name, os.release)

function BUILDER:objdir(arch, ...)
   local temp = pathcat(platform_dir, arch, mode)
   return pathcat('objects', temp:hash(true), ...)
end

function BUILDER:libdir(arch, ...)
   return pathcat(platform_dir, arch, mode, 'lib', ...)
end

function BUILDER:bindir(arch, ...)
   return pathcat(platform_dir, arch, mode, 'bin', ...)
end

function BUILDER:deps_files()
   return table.imap(self.archs, function(arch)
      return self:objdir(arch, 'graph.dump')
   end)
end

function BUILDER:setup()
   self:init()

   if mode == 'Release' then
      self:setup_c_compiler('fast', 'static')
   else
      self:setup_c_compiler('debug', 'static')
   end

   if os.args.archs then
      if os.args.archs == 'all' then
         self.archs = table.keys(self.CC.archs)
      else
         self.archs = os.args.archs:split(',')
      end
   end
end

function BUILDER:load()
   self:loadgraph(self:deps_files())

   local platform_source = nil
   local platform_defines = nil
   local platform_extra_libraries = nil

   if os.name == 'Windows' then
      platform_source = 'src/win32.c'
      platform_defines = {}
      platform_extra_libraries = {}
   else
      platform_source = 'src/posix.c'
      platform_defines = {
         'LUA_USE_POSIX',
         'LUA_USE_DLOPEN',
      }
      platform_extra_libraries = { 'dl', 'm' }
   end

   self:add_static_library({
      output = 'common',
      installdir = false,
      sources = {
      -- core lua
         'lua/src/lapi.c',
         'lua/src/lcode.c',
         'lua/src/ldebug.c',
         'lua/src/ldo.c',
         'lua/src/ldump.c',
         'lua/src/lfunc.c',
         'lua/src/lgc.c',
         'lua/src/llex.c',
         'lua/src/lmem.c',
         'lua/src/lobject.c',
         'lua/src/lopcodes.c',
         'lua/src/lparser.c',
         'lua/src/lstate.c',
         'lua/src/lstring.c',
         'lua/src/ltable.c',
         'lua/src/ltm.c',
         'lua/src/lundump.c',
         'lua/src/lvm.c',
         'lua/src/lzio.c',
      -- libraries
         'lua/src/lauxlib.c',
         'lua/src/lbaselib.c',
         'lua/src/ldblib.c',
         'lua/src/liolib.c',
         'lua/src/lmathlib.c',
         'lua/src/loslib.c',
         'lua/src/lstrlib.c',
         'lua/src/ltablib.c',
         'lua/src/loadlib.c',
         'lua/src/linit.c',
      -- common
         'src/os.c',
         'src/string.c',
         'src/table.c',
         'src/template.c',
         'src/utils.c',
         platform_source,
      },
      preprocessor_defines = platform_defines,
      preprocessor_includes = {
         'lua/src',
      },
   })

   self:add_executable({
      output = 'build',
      sources = {
         'src/build.c',
         'src/builder.c',
      },
      preprocessor_includes = {
         'lua/src',
      },
      compiler_flags = {
         self.CC.warnings_all,
      },
      libraries = {
         'common',
      },
      extra_libraries = platform_extra_libraries,
   })
   self:add_executable({
      output = 'lpp',
      sources = {
         'src/lpp.c',
      },
      preprocessor_includes = {
         'lua/src',
      },
      compiler_flags = {
         self.CC.warnings_all,
      },
      libraries = {
         'common',
      },
      extra_libraries = platform_extra_libraries,
   })

end

function BUILDER:save()
   self:savegraph(self:deps_files(), function(self, target, tag)
      io.stderr:write(string.format("deleting '%s'\n", target))
      os.rmdir({target, target:setext('.d')})
   end)
end

