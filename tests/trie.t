
#include <stdlib.h>
#include <string.h>

## function define_tire(name, key, value, parent)
##    local node = name .. '_node_t'
##    local branch = name .. '_branch_t'

typedef struct <$node$> <$node$>;
typedef struct <$branch$> <$branch$>;

void <$name$>_dealloc(<$branch$> *branch);

const <$node$> *<$name$>_get(const <$branch$> *branch, const <$key$> key);
<$node$> *<$name$>_put(<$branch$> **branch, const <$key$> key);

const <$node$> *<$name$>_find(const <$branch$> *branch,
                              const <$key$> *key, size_t len);
<$node$> *<$name$>_insert(<$branch$> **branch,
                          const <$key$> *key, size_t len);

struct <$node$>
{
   <$key$> key;
   <$value$> value;
   <$branch$> *children;
};

struct <$branch$>
{
   int count;
   <$node$> nodes[1];
};

## end

## function implement_tire(name, key, value)
##    local node = name .. '_node_t'
##    local branch = name .. '_branch_t'

void <$name$>_dealloc(<$branch$> *branch)
{
   if(branch != NULL)
   {
      int i;
      for(i = 0; i < branch->count; i += 1)
         <$name$>_dealloc(branch->nodes[i].children);
      free(branch);
   }
}

static int <$name$>_ifind(const <$branch$> *branch, const <$key$> key)
{
   int a = 0, b = branch->count;
   while(a < b)
   {
      int c = a + (b - a) / 2;
      if(branch->nodes[c].key < key)
         a = c + 1;
      else
         b = c;
   }
   return a;
}

const <$node$> *<$name$>_get(const <$branch$> *branch, const <$key$> key)
{
   if(branch != NULL)
   {
      int idx = <$name$>_ifind(branch, key);
      if(idx < branch->count && branch->nodes[idx].key == key)
         return branch->nodes + idx;
   }
   return NULL;
}

<$node$> *<$name$>_put(<$branch$> **branch, const <$key$> key)
{
   if((*branch) != NULL)
   {
      int idx = <$name$>_ifind((*branch), key);
      if(idx == (*branch)->count || (*branch)->nodes[idx].key != key)
      {
         int i;
         /* leak on out of memory */
         size_t len = sizeof(<$branch$>) + sizeof(<$node$>) * (*branch)->count;
         (*branch) = (<$branch$> *)realloc((*branch), len);
         if((*branch) == NULL)
            return NULL;

         for(i = (*branch)->count; i > idx; i -= 1)
            (*branch)->nodes[i] = (*branch)->nodes[i - 1];
         (*branch)->count += 1;
         
         memset((*branch)->nodes + idx, 0, sizeof(<$node$>));
         (*branch)->nodes[idx].key = key;
      }
      return (*branch)->nodes + idx;
   }
   else
   {
      int idx = 0;
      (*branch) = (<$branch$> *)malloc(sizeof(<$branch$>));
      if((*branch) == NULL)
         return NULL;

      (*branch)->count = 1;

      memset((*branch)->nodes + idx, 0, sizeof(<$node$>));
      (*branch)->nodes[idx].key = key;

      return (*branch)->nodes + idx;
   }
}

const <$node$> *<$name$>_find(const <$branch$> *branch,
                              const <$key$> *key, size_t len)
{
   const <$node$> *node = NULL;
   while(branch != NULL && len-- > 0)
   {
      node = <$name$>_get(branch, *key++);
      if(node == NULL)
         return NULL;

      branch = node->children;
   }
   return node;
}

<$node$> *<$name$>_insert(<$branch$> **branch, const <$key$> *key, size_t len)
{
   <$node$> *node = NULL;
   while(len-- > 0)
   {
      node = <$name$>_put(branch, *key++);
      if(node == NULL)
         return NULL;

      branch = &node->children;
   }
   return node;
}

## end
