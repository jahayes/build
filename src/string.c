/* see copyright notice in common.h */

#include "common.h"

#include <stdlib.h>
#include <string.h>

#define issep(c) ((c) == '/')
#define isdot(c) ((c) == '.')

char *normalize_path(const char *input, char *output, int removelastsep)
{
   char *dst, *head = output;
   const char *src;
#ifdef _WIN32
   /* change '\' to '/' */
   src = input;
   dst = output;
   while(*src != '\0')
   {
      char v = *src++;
      if(v == '\\')
         v = '/';
      *dst++ = v;
   }
   *dst = 0;

   if(issep(head[0]) && issep(head[1]))         /* UNC paths */
      head += 1;
   else if(isalpha(head[0]) && head[1] == ':')  /* drives */
      head += 2;

   src = dst = head;
#else
   src = input;
   dst = head;
#endif
   /* remove "//" */
   while(*src != '\0')
   {
      if((*dst++ = *src++) == '/')
      {
         while(issep(*src))
            src += 1;
      }
   }
   *dst = 0;

   if(issep(*head))
      head += 1;

   /* remove leading "./", "/./" and trailing "/." */
   src = dst = head;
   while(isdot(src[0]) && issep(src[1]))
      src += 2;
   while(*src != '\0')
   {
      if(issep(src[0]) && isdot(src[1]) && (issep(src[2]) || src[2] == '\0'))
         src += 2;
      else
         *dst++ = *src++;
   }
   *dst = '\0';
   
   /* remove "/../" and trailing "/.." */
   src = dst = head;
   while(isdot(src[0]) && isdot(src[1]) && issep(src[2]))
      src = dst = head += 3;
   while(*src != '\0')
   {
      if(issep(src[0]) && isdot(src[1]) && isdot(src[2]) &&
         (issep(src[3]) || src[3] == '\0'))
      {
         src += 3;
         if(dst == head)
         {
            *dst++ = '.';
            *dst++ = '.';
            *dst++ = '/';
            head += 3;
         }
         else
         {
            do
               dst--;
            while(dst > head && !issep(*dst));
            if(dst == head && issep(*src))
               src += 1;
         }
      }   
      else
         *dst++ = *src++;
   }
   *dst-- = '\0';

   if(removelastsep && issep(*dst))
      *dst = '\0';
   
   return output;
}

/* string.cleanup(path (1)) */
static int string_cleanup(lua_State *L)
{
   size_t len;
   const char *input = luaL_checklstring(L, 1, &len);
   char *output = malloc(len + 1);
   if(output == NULL)
      return oserror(L, "malloc()");
   normalize_path(input, output, 1);
   lua_pushstring(L, output);
   free(output);
   return 1;
}

/* string.dirname(path (1)) */
/* Acts like the shell's dirname */
static int string_dirname(lua_State *L)
{
   size_t len;
   const char *path = luaL_checklstring(L, 1, &len);
   if(path == NULL || len == 0)
      lua_pushliteral(L, ".");
   else
   {
      char *tail, *work = malloc(len + 1);
      if(work == NULL)
         return oserror(L, "malloc()");
      normalize_path(path, work, 1);
      tail = work + strlen(work) - 1;

      /* remove trailing slashes */
      while(tail > work && issep(*tail))
         tail -= 1;
      /* remove basename */
      while(tail > work && !issep(*tail))
         tail -= 1;
      /* remove trailing slashes */
      while(tail > work && issep(*tail))
         tail -= 1;
      
      if(tail == work)
         lua_pushstring(L, issep(*tail) ? "/" : ".");
      else
         lua_pushlstring(L, work, (size_t)(tail - work + 1));
      free(work);
   }
   return 1;
}

/* string.basename(path (1)) */
/* Acts like the shell's basename */
static int string_basename(lua_State *L)
{
   size_t len;
   const char *path = luaL_checklstring(L, 1, &len);
   if(path == NULL || len == 0)
      lua_pushliteral(L, ".");
   else
   {
      char *tail, *work = malloc(len + 1);
      if(work == NULL)
         return oserror(L, "malloc()");
      normalize_path(path, work, 1);
      tail = work + strlen(work) - 1;

      /* remove trailing slashes */
      while(tail > work && issep(*tail))
         tail -= 1;
      
      if(tail == work && issep(*tail))
         lua_pushliteral(L, "/");
      else
      {
         char *head = tail;
         while(head > work && !issep(head[-1]))
            head -= 1;
         
         lua_pushlstring(L, head, (size_t)(tail - head + 1));
      }
      free(work);
   }
   return 1;
}

/* string.getext(str (1)) */
static int string_getext(lua_State *L)
{
   const char *str = luaL_checkstring(L, 1);
   char *dot = strrchr(str, '.');
   if(dot == NULL)
      lua_pushliteral(L, "");
   else
      lua_pushstring(L, dot);
   return 1;
}

/* string.setext(str (1), ext (2)) */
static int string_setext(lua_State *L)
{
   const char *str = luaL_checkstring(L, 1);
   const char *ext = luaL_checkstring(L, 2);
   char *dot = strrchr(str, '.');
   
   luaL_Buffer b;
   luaL_buffinit(L, &b);
   if(dot == NULL)
      luaL_addstring(&b, str);
   else
      luaL_addlstring(&b, str, (size_t)(dot - str));
   luaL_addstring(&b, ext);
   luaL_pushresult(&b);
   return 1;
}


typedef unsigned int hash_t;
static hash_t hash_string(const char *str, size_t len)
{
#define R(x, k) ((x << k) | (x >> (32 - k)))
#define S(idx, bits) ((hash_t)(pos[idx]) << bits)
   hash_t a, b, c;
   const unsigned char *pos = (const unsigned char *)str;
   
   a = b = c = 0xdeadbeef + (hash_t)len;
   
   while(len > 12)
   {
      a += S( 0, 0) | S( 1, 8) | S( 2, 16) | S( 3, 24);
      b += S( 4, 0) | S( 5, 8) | S( 6, 16) | S( 7, 24);
      c += S( 8, 0) | S( 9, 8) | S(10, 16) | S(11, 24);
      
      a -= c;  a ^= R(c,  4);  c += b;
      b -= a;  b ^= R(a,  6);  a += c;
      c -= b;  c ^= R(b,  8);  b += a;
      a -= c;  a ^= R(c, 16);  c += b;
      b -= a;  b ^= R(a, 19);  a += c;
      c -= b;  c ^= R(b,  4);  b += a;
      
      len -= 12;
      pos += 12;
   }
   
   switch(len)
   {
      case 12: c += S(11, 24);
      case 11: c += S(10, 16);
      case 10: c += S( 9, 8);
      case  9: c += S( 8, 0);
      case  8: b += S( 7, 24);
      case  7: b += S( 6, 16);
      case  6: b += S( 5, 8);
      case  5: b += S( 4, 0);
      case  4: a += S( 3, 24);
      case  3: a += S( 2, 16);
      case  2: a += S( 1, 8);
      case  1: a += S( 0, 0);
         
         c ^= b; c -= R(b, 14);
         a ^= c; a -= R(c, 11);
         b ^= a; b -= R(a, 25);
         c ^= b; c -= R(b, 16);
         a ^= c; a -= R(c,  4);
         b ^= a; b -= R(a, 14);
         c ^= b; c -= R(b, 24);
         
      default:
         return c;
   }
#undef R
#undef S
}

/* string.hash(str (1)[, asstring(2)) */
static int string_hash(lua_State *L)
{
   size_t len;
   const char *str = luaL_checklstring(L, 1, &len);
   hash_t hash = hash_string(str, len);

   if(lua_toboolean(L, 2))
   {
      const static char map[16] = "0123456789abcdef";
      char ret[8];
      ret[7] = map[0xf & hash]; hash >>= 4;
      ret[6] = map[0xf & hash]; hash >>= 4;
      ret[5] = map[0xf & hash]; hash >>= 4;
      ret[4] = map[0xf & hash]; hash >>= 4;
      ret[3] = map[0xf & hash]; hash >>= 4;
      ret[2] = map[0xf & hash]; hash >>= 4;
      ret[1] = map[0xf & hash]; hash >>= 4;
      ret[0] = map[0xf & hash];
      lua_pushlstring(L, ret, 8);
   }
   else
      lua_pushnumber(L, hash);
   return 1;
}

void append_string(lua_State *L, int debug)
{
   static const struct luaL_reg functions[] =
   {
      { "cleanup", string_cleanup },
      { "dirname", string_dirname },
      { "basename", string_basename },
      { "getext", string_getext },
      { "setext", string_setext },
      { "hash", string_hash },
      { NULL, NULL }
   };
   register_functions(L, LUA_STRLIBNAME, functions, debug);
}
