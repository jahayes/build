/* see copyright notice in common.h */

#include "common.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

void error(const char *fmt, ...)
{
   va_list args;
   va_start(args, fmt);
   vfprintf(stderr, fmt, args);
   va_end(args);
}

void message(const char *fmt, ...)
{
   va_list args;
   va_start(args, fmt);
   vfprintf(stdout, fmt, args);
   va_end(args);
}

void dump_stack(lua_State *L)
{
   int i, top = lua_gettop(L);
   for(i = 1; i <= top; i++)
   {
      int type = lua_type(L, i);
      switch(type)
      {
      case LUA_TSTRING:
         error("'%s'", lua_tostring(L, i));
         break;
      case LUA_TBOOLEAN:
         error("%s", lua_toboolean(L, i) ? "true" : "false");
         break;
      case LUA_TNUMBER:
         error("%g", lua_tonumber(L, i));
         break;
      default:
         error("%s (%p)", lua_typename(L, type), lua_topointer(L, i));
         break;
      }
      error("  ");
   }
   error("\n");
}

void register_functions(lua_State *L, const char *table,
                        const struct luaL_reg *functions, int debug)
{
   if(table == NULL)
      lua_pushvalue(L, LUA_GLOBALSINDEX);
   luaL_register(L, table, functions);
   lua_pop(L, 1);
   if(debug)
   {
      if(table == NULL)
      {
         while(functions->name != NULL)
            error("*S* adding %s()\n", (functions++)->name);
      }
      else
      {
         while(functions->name != NULL)
            error("*S* adding %s.%s()\n", table, (functions++)->name);
      }
   }
}

void register_methods(lua_State *L, const char *metatable,
                      const struct luaL_reg *methods, int debug)
{
   if(luaL_newmetatable(L, metatable))
   {
      /* metatable.__index = metatable */
      lua_pushliteral(L, "__index");
      lua_pushvalue(L, -2);
      lua_rawset(L, -3);
   }
   luaL_register(L, NULL, methods);
   lua_setglobal(L, metatable);
   if(debug)
   {
      while(methods->name != NULL)
         error("*S* adding %s:%s()\n", metatable, (methods++)->name);
   }
}

static int global_index(lua_State *L)
{
   error("*U* variable '%s' not found\n", lua_tostring(L, 2));
   lua_pushnil(L);
   return 1;
}

static int global_newindex(lua_State *L)
{
   error("*U* adding new variable '%s'\n", lua_tostring(L, 2));
   lua_rawset(L, 1);
   return 0;
}

void warn_undefined_globals(lua_State *L)
{
   lua_createtable(L, 0, 2);
   lua_pushcclosure(L, global_index, 0);
   lua_setfield(L, -2, "__index");
   lua_pushcclosure(L, global_newindex, 0);
   lua_setfield(L, -2, "__newindex");
   lua_setmetatable(L, LUA_GLOBALSINDEX);
}

static void insert_table(lua_State *L, const char *table, const char *key)
{
   lua_createtable(L, 0, 0);
   lua_getglobal(L, table);
   lua_pushvalue(L, -2);
   lua_setfield(L, -2, key);
   lua_pop(L, 1);
}


void load_args(lua_State *L, int argc, const char *argv[], int debug)
{
   int i;
   insert_table(L, LUA_OSLIBNAME, "args");
   for(i = 0; i < argc; i++)
   {
      if(argv[i] != NULL && argv[i][0] == '-' && argv[i][1] == '-')
      {
         char *key = (char *)argv[i] + 2;
         char *value = strchr(key, '=');
         if(value != NULL)
         {
            while(*value == '=')
               *value++ = '\0';
            
            if(debug)
               error("*S* setting os.args[%s] = '%s'\n", key, value);
            
            lua_pushstring(L, value);
         }
         else
         {
            if(debug)
               error("*S* setting os.args[%s] = true\n", key);
            
            lua_pushboolean(L, 1);
         }
         lua_setfield(L, -2, key);
         argv[i] = NULL;
      }
   }
   lua_pop(L, 1);
}

void load_env(lua_State *L, const char *env[], int debug)
{
   int i;
   insert_table(L, LUA_OSLIBNAME, "env");
   for(i = 0; env[i] != NULL; i++)
   {
      const char *value = strchr(env[i], '=');
      if(value != NULL)
      {
         size_t len = (size_t)(value - env[i]);
         char *key = calloc(len + 1, sizeof(char));
         memcpy(key, env[i], len);
         /* windows likes to call PATH Path */
         if(strcmp(key, "Path") == 0)
            strcpy(key, "PATH");

         while(*value == '=')
            value += 1;
   
         if(debug)
            error("*S* setting os.env[%s] = '%s'\n", key, value);

         lua_pushstring(L, value);
         lua_setfield(L, -2, key);
         free(key);
      }
   }
   lua_pop(L, 1);
}

void update_packagepath(lua_State *L, int debug)
{
   size_t i, j;
   static const char *paths[] = {
      ".",
      "config",
      "scripts",
      "templates",
   };
   static const char *suffix[] = {
      LUA_DIRSEP LUA_PATH_MARK ".lua" LUA_PATHSEP,
      LUA_DIRSEP LUA_PATH_MARK LUA_DIRSEP "init.lua" LUA_PATHSEP,
   };
   luaL_Buffer path;
   luaL_buffinit(L, &path);
   
   for(i = 0; i < ARRAY_LENGTH(paths); i += 1)
   {
      for(j = 0; j < ARRAY_LENGTH(suffix); j += 1)
      {
         luaL_addstring(&path, paths[i]);
         luaL_addstring(&path, suffix[j]);
      }
   }
   
   lua_getglobal(L, LUA_LOADLIBNAME);
   lua_getfield(L, -1, "path");
   luaL_addvalue(&path);
   luaL_pushresult(&path);
   if(debug)
      error("*S* setting package.path = '%s'\n", lua_tostring(L, -1));
   lua_setfield(L, -2, "path");
   lua_pop(L, 1);
}

int call_hook(lua_State *L, const char *name, int debug)
{
   /* stack:
    * 1) debug.traceback
    * 2) builder
    */
   int ret = 0;
   lua_settop(L, 2);
   luaL_getmetatable(L, BUILDER_METATYPE_NAME);
   lua_getfield(L, 3, name);       /* 3 */
   lua_remove(L, 3);
   if(!lua_isnoneornil(L, 3))
   {
      if(debug)
         error("*S* running BUILDER:%s()\n", name);
      
      lua_pushvalue(L, 2);
      if(lua_pcall(L, 1, 0, 1))
      {
         const char *err = lua_tostring(L, -1);
         error("error running BUILDER:%s():\n%s\n", name, err);
         ret = 1;
      }
   }
   lua_settop(L, 2);
   return ret;
}

static int find_opt(const char **arg, int opt)
{
   char *pos;
   if(*arg == NULL || (*arg)[0] != '-' || (*arg)[1] == '-')
      return 0;
   
   if((pos = strchr(*arg, opt)) == NULL)
      return 0;
   
   while(*pos != '\0')
   {
      pos[0] = pos[1];
      pos++;
   }
   if((*arg)[1] == '\0')
      *arg = NULL;
   return 1;
}

const char *find_arg(int argc, const char *argv[],
                     int opt, const char *value)
{
   int i;
   for(i = 1; i < argc; i++)
   {
      if(find_opt(argv + i, opt))
      {
         int j = i + 1;
         if(j < argc && argv[j] != NULL && *argv[j] != '-')
         {
            value = argv[j];
            argv[j] = NULL;
            break;
         }
         else
         {
            error("option '%c' needs a value\n", opt);
            exit(EXIT_FAILURE);
         }
      }
   }
   return value;
}

int find_flag(int argc, const char *argv[], int opt)
{
   int i;
   for(i = 1; i < argc; i++)
   {
      if(find_opt(argv + i, opt))
         return 1;
   }
   return 0;
}

int compact_args(int argc, const char *argv[])
{
   int src, dst;
   for(src = 0, dst = 0; src < argc; src += 1)
   {
      if(argv[src] != NULL)
         argv[dst++] = argv[src];
   }
   return dst;
}

