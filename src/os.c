/* see copyright notice in common.h */

#include "common.h"

#include <stdlib.h>
#include <string.h>

/* os.run(argv (1)[, workingdir (2)[, env(3)]]) */
static int os_run(lua_State *L)
{
   int ret;
   job_t job;
   if((job = job_exec(L, 0)) == JOB_NULL)
      return oserror(L, "job_exec()");

   if(job_wait(job, &ret, 1) != job)
      return oserror(L, "job_wait()");
   
   lua_pushinteger(L, ret);
   return 1;
}

/* os.which(...) */
static int os_which(lua_State *L)
{
   const char *path = NULL;
   char full[MAXPATH] = "";
   int i, n = lua_gettop(L);
   
   lua_getglobal(L, LUA_OSLIBNAME);
   lua_getfield(L, -1, "env");
   lua_getfield(L, -1, "PATH");
   path = lua_tostring(L, -1);
   lua_pop(L, 3);
   if(path == NULL)
      return luaL_error(L, "PATH not set");
   
   for(i = 1; i <= n; i += 1)
   {
      if(find_first_program(L, path, full, i))
      {
         lua_pushstring(L, full);
         return 1;
      }
   }
   return 0;
}

/* os.mkdir(...) */
static int os_mkdir(lua_State *L)
{
   int i, n = lua_gettop(L);
   for(i = 1; i <= n; i += 1)
   {
      if(make_directory(L, 1))
         return oserror(L, "mkdir()");
   }
   return 0;
}

/* os.rmdir(...) */
static int os_rmdir(lua_State *L)
{
   int i, n = lua_gettop(L);
   for(i = 1; i <= n; i += 1)
   {
      if(remove_directory(L, i))
         return oserror(L, "rmdir()");
   }
   return 0;
}

/* os.glob(...) */
static int os_glob(lua_State *L)
{
   int i, n = lua_gettop(L) + 1;
   lua_createtable(L, 0, 0);
   lua_insert(L, 1);
   for(i = 2; i <= n; i += 1)
   {
      if(glob_pattern(L, i))
         return oserror(L, "glob()");
   }
   lua_settop(L, 1);
   return 1;
}

void append_os(lua_State *L, int debug)
{
   static const struct luaL_reg functions[] =
   {
      { "run", os_run },
      { "which", os_which },
      { "mkdir", os_mkdir },
      { "rmdir", os_rmdir },
      { "glob", os_glob },

      { "eval", os_eval },
      { "cwd", os_cwd },
      { "stat", os_stat },
      { NULL, NULL }
   };
   register_functions(L, LUA_OSLIBNAME, functions, debug);
}
