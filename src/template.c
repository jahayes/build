/* see copyright notice in common.h */

#include "common.h"

#include <stdlib.h>

#include <string.h>
#include <ctype.h>
#include <errno.h>

struct buffer
{
   size_t size;
   size_t len;
   char *data;
};

static void grow_buffer(struct buffer *buf, size_t len)
{
   size_t growsize = 0xFFFF;
   size_t newsize = (buf->len + len + growsize) & ~growsize;
   if(buf->size < newsize)
   {
      buf->size = newsize;
      buf->data = realloc(buf->data, newsize + 1);
   }
}

#define raw_char(b, c) b->data[b->len++] = (char)(c)
static void push_char(struct buffer *buf, int c)
{
   grow_buffer(buf, 1ul);
   raw_char(buf, c);
}

#define raw_string(b, s, l) \
   do { memcpy(b->data + b->len, s, l); b->len += l; } while(0)

static void push_string(struct buffer *buf, const char *str, size_t len)
{
   grow_buffer(buf, len);
   raw_string(buf, str, len);
}

static void raw_integer(struct buffer *buf, int value)
{
   char *end, *start = buf->data + buf->len;
   while(value != 0)
   {
      int digit = value % 10;
      value /= 10;
      raw_char(buf, '0' + digit);
   }
   end = buf->data + buf->len - 1;
   while(start < end)
   {
      char temp = *start;
      *start++ = *end;
      *end-- = temp;
   }
}

static void dump_accumulator(struct buffer *script, struct buffer *acc)
{
   if(acc->len != 0)
   {
      char *str = acc->data;
      size_t len = acc->len;
      
      grow_buffer(script, 9 + 2 * len + 3);
      raw_string(script, "__write(\"", 9ul);
      while(len--)
      {
         char c = *str++;
         if(c == '\n' || c == '\r')
         {
            raw_char(script, '\\');
            raw_char(script, c == '\n' ? 'n' : 'r');
         }
         else
         {
            if(c == '"' || c == '\\')
               raw_char(script, '\\');
            raw_char(script, c);
         }
      }
      raw_string(script, "\")\n", 3ul);
      acc->len = 0;
   }
}

static void track_linenumber(struct buffer *buf, const char *inname, int line)
{
   size_t len = strlen(inname);
   grow_buffer(buf, 6 + 10 + 2 + len + 2);
   raw_string(buf, "#line ", 6ul);
   raw_integer(buf, line);
   raw_string(buf, " \"", 2ul);
   raw_string(buf, inname, len);
   raw_string(buf, "\"\n", 2ul);
}

static FILE *outfile = NULL;
static int setoutputfile(lua_State *L)
{
   const char *outname = luaL_optstring(L, 1, NULL);

   if(outfile != NULL)
      fclose(outfile);

   outfile = NULL;
   if(outname != NULL && *outname != '\0')
   {
      if((outfile = fopen(outname, "wt")) == NULL)
         return luaL_error(L, "%s: %s", outname, strerror(errno));
   }
      
   return 0;
}

static int __write(lua_State *L)
{
   if(outfile != NULL)
   {
      size_t len;
      const char *str = luaL_checklstring(L, 1, &len);
      
      if(fwrite(str, sizeof(char), len, outfile) != len)
         return luaL_error(L, "%s", strerror(errno));
   }
   return 0;
}





static int include(lua_State *L)
{
   static char tracking_stack[64] = { 1 };
   static int tracking_depth = 0;

   char line[1024], *pos;
   char block = '\0';
   int linenumber = 0;
   struct buffer acc = { 0, 0, NULL };
   struct buffer script = { 0, 0, NULL };

   const char *inname = luaL_checkstring(L, 1);
   int track = luaL_optboolean(L, 2, tracking_stack[tracking_depth]);

   FILE *infile = fopen(inname, "rt");
   if(infile == NULL)
      return luaL_error(L, "%s: %s", inname, strerror(errno));

   tracking_stack[tracking_depth += 1] = track;

   if(track)
      track_linenumber(&acc, inname, linenumber + 1);

   while((pos = fgets(line, (int)sizeof(line), infile)) != NULL)
   {
      linenumber += 1;
      if(pos[0] == '#' && pos[1] == '#')
      {
         if(block != '\0')
         {
            error("%s:%d:%d: error: nested blocks\n",
                  inname, linenumber - 1, (int)(pos - line));
         }

         dump_accumulator(&script, &acc);
         do
         {
            push_string(&acc, pos + 2, strlen(pos + 2));
            pos = fgets(line, (int)sizeof(line), infile);
            linenumber += 1;
         }
         while(pos != NULL && pos[0] == '#' && pos[1] == '#');

         push_string(&script, acc.data, acc.len);
         acc.len = 0;

         if(pos == NULL)
            break;

         if(track)
            track_linenumber(&acc, inname, linenumber);
      }

      while(*pos != '\0')
      {
         if(pos[0] == '<' && (pos[1] == '#' || pos[1] == '$'))
         {
            if(block != '\0')
            {
               error("%s:%d:%d: error: nested blocks\n",
                     inname, linenumber - 1, (int)(pos - line));
            }
            
            block = pos[1];
            pos += 2;
            
            dump_accumulator(&script, &acc);
            if(block == '$')
               push_string(&acc, "__write(", 8ul);
         }
         else if(pos[1] == '>' && (pos[0] == '#' || pos[0] == '$'))
         {
            char *end;
            if(block != pos[0])
            {
               message("%s:%d:%d: warning: block tag mismatch\n",
                       inname, linenumber - 1, (int)(pos - line));
            }
            pos += 2;
            
            if(block == '$')
               push_char(&acc, ')');
            
            push_char(&acc, '\n');
            push_string(&script, acc.data, acc.len);
            acc.len = 0;
            
            end = pos;
            while(isspace(*end))
               end += 1;
            
            if(*end == '\0')
            {
               push_string(&acc, pos, (size_t)(end - pos));
               if(track)
                  track_linenumber(&acc, inname, linenumber + 1);
               pos = end;
            }
            block = '\0';
         }
         else
         {
            push_char(&acc, *pos);
            pos += 1;
         }
      }
   }
   fclose(infile);
   
   dump_accumulator(&script, &acc);
   free(acc.data);
   
   /*fwrite(script.data, sizeof(char), script.len, stderr);*/
   if(luaL_loadbuffer(L, script.data, script.len, inname))
   {
      /*fwrite(script.data, sizeof(char), script.len, stderr);*/
      tracking_depth -= 1;
      return luaL_error(L, "%s", lua_tostring(L, -1));
   }
   free(script.data);
   
   lua_call(L, 0, 0);
   tracking_depth -= 1;
   return 0;
}

static int resourcepath(lua_State *L)
{
   const char *end, *name = luaL_checkstring(L, 1);
   lua_Debug ar;
   luaL_Buffer path;

   lua_getstack(L, 1, &ar);
   lua_getinfo(L, "S", &ar);
   
   end = strrchr(ar.source, '/');
   if(end == NULL)
      end = strrchr(ar.source, '\\');
   
   luaL_buffinit(L, &path);
   luaL_addlstring(&path, ar.source + 1, (size_t)(end - ar.source));
   luaL_addstring(&path, name);
   luaL_pushresult(&path);
   return 1;
}

void open_templates(lua_State *L, int debug)
{
   static const struct luaL_reg functions[] =
   {
      { "setoutputfile", setoutputfile },
      { "__write", __write },
      { "include", include },
      { "resourcepath", resourcepath },
      { NULL, NULL }
   };
   register_functions(L, NULL, functions, debug);
}
