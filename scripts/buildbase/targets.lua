-- see copyright notice in buildbase/init.lua

local tgfv = table.getfirstvalue
local tgav = table.getallvalues

local function delete_files(self, args, output, deps)
   os.rmdir(args)
   return 0
end

local function install_file(self, args, output, deps)
   makeoutputdir(output)
   local ret = self:run({'cp', deps[1], output})
   if ret == 0 and tgfv(args, 'run_strip') then
      ret = self:run({'strip', output})
   end
   if ret == 0 and tgfv(args, 'run_ranlib') then
      ret = self:run({'ranlib', output})
   end
   return ret
end

function BUILDER:_add_target(args, targethandler, output, outdir)
   local parts = {}
   local itarget = nil
   local cparts = {}
   local coutput = 'clean-' .. output
   for tag = 1, #self.archs do
      local arch = self.archs[tag]
      local target = outdir(self, arch, output)
      local objdir = self:objdir(arch, output:hash(true))

      local function converter(args)
         return function(arg)
            if type(arg) == 'table' then
               assert(arg.defaults == nil or arg.defaults == args,
                      'defaults in source groups must be nil')
               arg.defaults = args
               return table.imap(arg.sources, converter(arg))
            else
               local handler = self.source_handlers[arg:getext()]
               return handler(self, arch, args, arg, objdir, tag)
            end
         end
      end
      local objs = table.imap(args.sources, converter(args))

      targethandler(arch, args, target, objs, tag)
      parts[#parts + 1] = target
      if arch == os.cputype then
         itarget = target
      end

      local temp = coutput .. '-' .. arch
      self:addgroup(temp, nil, delete_files, {target, objdir})
      cparts[#cparts + 1] = temp;
   end

   if self.combiner ~= nil and #parts > 1 then
      local target = outdir(self, 'universal', output)
      self:addfile(target, parts, self.combiner, args, #parts + 1)
      parts[#parts + 1] = target
      itarget = target

      local temp = coutput .. '-universal'
      self:addgroup(temp, nil, delete_files, {target})
      cparts[#cparts + 1] = temp;
   end

   self:addgroup(output, parts)
   self:addgroup('all', {output})

   if itarget and tgfv(args, 'installdir') then
      local target = pathcat(tgfv(args, 'installdir'), output)
      self:addfile(target, {itarget}, install_file, args)
      self:addgroup('install', {target})
   end

   self:addgroup(coutput, cparts)
   self:addgroup('clean', {coutput})
   return parts
end

function BUILDER:_set_install_directory(args, dir, strip, ranlib)
   if tgfv(args, 'installdir') == nil then
      args.installdir = self:installdir(dir)
   end
   if tgfv(args, 'run_strip') == nil then
      args.run_strip = strip
   end
   if tgfv(args, 'run_ranlib') == nil then
      args.run_ranlib = ranlib
   end
end

local function get_linker_type(args)
   return tgfv(args, 'linker_type') or '.c'
end

function BUILDER:add_static_library(args)
   local output = self:static_archive_name(tgfv(args, 'output'))
   self:_set_install_directory(args, 'lib', false, true)

   local ext = get_linker_type(args)
   local function targethandler(arch, args, target, objs, tag)
      local action = self.static_linkers[arch .. ext]
      self:addfile(target, objs, action, args, -tag)
   end

   return self:_add_target(args, targethandler, output, self.libdir)
end

function BUILDER:_get_libraries(args, arch)
   return table.imap(tgav(args, 'libraries'), function(arg)
      if arg:find('/') then
         return arg
      else
         return self:libdir(arch, self:static_archive_name(arg))
      end
   end)
end

function BUILDER:add_shared_library(args)
   local output = self:shared_library_name(tgfv(args, 'output'),
                                           tgfv(args, 'version'))
   self:_set_install_directory(args, 'lib', false, false)

   local ext = get_linker_type(args)
   local function targethandler(arch, args, target, objs, tag)
      local libs = self:_get_libraries(args, arch)
      local action = self.shared_linkers[arch .. ext]
      self:addfile(target, {objs, libs}, action, args, -tag)
   end

   return self:_add_target(args, targethandler, output, self.libdir)
end

function BUILDER:add_executable(args)
   local output = self:executable_name(tgfv(args, 'output'))
   self:_set_install_directory(args, 'bin', true, false)

   local ext = get_linker_type(args)
   local function targethandler(arch, args, target, objs, tag)
      local libs = self:_get_libraries(args, arch)
      local action = self.executable_linkers[arch .. ext]
      self:addfile(target, {objs, libs}, action, args, -tag)
   end

   return self:_add_target(args, targethandler, output, self.bindir)
end

