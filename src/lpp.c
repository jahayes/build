/* see copyright notice in common.h */

#include "common.h"

#include "lualib.h"

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

#define PROGRAM    "lpp"
#define VERSION    "0.1.0"
#define COPYRIGHT  "Copyright (C) 2010-2011 John A. Hayes"
#define WEBSITE    "http://bitbucket.org/jahayes/build"

static void usage(int argc, const char *argv[])
{
   int count = argc;
   if(find_flag(argc, argv, 'v'))
   {
      message(PROGRAM" "VERSION" "COPYRIGHT"\n"
              LUA_RELEASE" "LUA_COPYRIGHT"\n"
              "\n"
              "  See "WEBSITE" for more information.\n");
      exit(EXIT_SUCCESS);
   }
   
   while(argc-- && (*argv == NULL || **argv != '-'))
      argv++;

   if(argc == -1 && count >= 3)
      return;
   else if(argc != -1)
      error(PROGRAM": illegal option -- %s\n", *argv + 1);

   error("usage: "PROGRAM" [-vs] [--KEY[=VALUE]]* <input>+ <output>\n");
   error("   -v         Print version information and exit.\n");
   error("   -s         Suppress adding the source file line numbers.\n");
   error("  --KEY       Set KEY in the os.args table to true.\n");
   error("  --KEY=VALUE Set KEY in the os.args table to 'VALUE'.\n");
   error("\n");
   error("  Debugging options: [-SU]\n");
   error("   -S         Log Lua setup.\n");
   error("   -U         Log undefined variable accesses.\n");
   error("\n");
   error("  See "WEBSITE" for more information.\n");
   exit(EXIT_FAILURE);
}

int main(int argc, const char *argv[], const char *env[])
{
   int i;
   lua_State *L = NULL;

   int ret = EXIT_SUCCESS;
   int track = !find_flag(argc, argv, 's');
   int setup = find_flag(argc, argv, 'S');
   int undefined = find_flag(argc, argv, 'U');

   if(setup)
      error("*S* creating lua state\n");
   
   L = luaL_newstate();
   luaL_openlibs(L);
   update_packagepath(L, setup);
   append_table(L, setup);
   append_string(L, setup);
   /*append_os(L, setup);*/
   open_templates(L, setup);
   load_args(L, argc, argv, setup);
   load_env(L, env, setup);

   if(undefined)
      warn_undefined_globals(L);

   argc = compact_args(argc, argv);
   usage(argc, argv);

   lua_settop(L, 0);
   lua_getglobal(L, "debug");
   lua_getfield(L, 1, "traceback");           
   lua_remove(L, 1);
   
   /* stack:
    * 1) debug.traceback
    */

   lua_getglobal(L, "setoutputfile");
   lua_pushstring(L, argv[argc - 1]);
   if(lua_pcall(L, 1, 0, 1) != 0)
   {
      error(PROGRAM": error:\n%s\n", lua_tostring(L, -1));
      ret = EXIT_FAILURE;
      goto cleanup;
   }

   for(i = 1; i < argc - 1; i += 1)
   {
      lua_getglobal(L, "include");
      lua_pushstring(L, argv[i]);
      lua_pushboolean(L, track);
      if(lua_pcall(L, 2, 0, 1) != 0)
      {
         error(PROGRAM": error:\n%s\n", lua_tostring(L, -1));
         ret = EXIT_FAILURE;
         goto cleanup;
      }
   }

   lua_getglobal(L, "setoutputfile");
   lua_pushnil(L);
   if(lua_pcall(L, 1, 0, 1) != 0)
   {
      error(PROGRAM": error:\n%s\n", lua_tostring(L, -1));
      ret = EXIT_FAILURE;
      goto cleanup;
   }
   
cleanup:
   lua_settop(L, 0);
   lua_close(L);
   return ret;
}

