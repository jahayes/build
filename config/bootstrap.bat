
cl /nologo /Ilua\src ^
  lua\src\lapi.c lua\src\lcode.c lua\src\ldebug.c lua\src\ldo.c ^
  lua\src\ldump.c lua\src\lfunc.c lua\src\lgc.c lua\src\llex.c lua\src\lmem.c ^
  lua\src\lobject.c lua\src\lopcodes.c lua\src\lparser.c lua\src\lstate.c ^
  lua\src\lstring.c lua\src\ltable.c lua\src\ltm.c lua\src\lundump.c ^
  lua\src\lvm.c lua\src\lzio.c lua\src\lauxlib.c lua\src\lbaselib.c ^
  lua\src\ldblib.c lua\src\liolib.c lua\src\lmathlib.c lua\src\loslib.c ^
  lua\src\ltablib.c lua\src\lstrlib.c lua\src\loadlib.c lua\src\linit.c ^
  src\build.c src\builder.c src\os.c src\string.c src\table.c ^
  src\template.c  src\utils.c src\win32.c ^
  /Febuild.exe && ^
.\build.exe -e --mode=Release --archs=all
del *.obj *.pdb build.exe


