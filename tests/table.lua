function BUILDER:load() self:addgroup('all') end

function dumpvalue(name, value)
   local out = io.stderr

   local function dump(t)
      if type(t) == 'table' then
         out:write('{')
         for k,v in pairs(t) do
            out:write(' [' .. tostring(k) .. '] = ')
            dump(v)
            out:write(', ')
         end
         out:write('}')
      else
         out:write(tostring(t))
      end
   end

   out:write(name .. ' = ')
   dump(value)
   out:write('\n')
end

local function test_imap(t1)
   dumpvalue('test_imap(before)', t1)
   local t2 = table.imap(t1, function(v) return v + 1 end)
   dumpvalue('test_imap(after)', t2)
end

local function test_map(t1)
   dumpvalue('test_map(before)', t1)
   local t2 =table.map(t1, function(v) return v + 1 end)
   dumpvalue('test_map(after)', t2)
end

test_imap({1,2,3,4,5})
test_map({1,2,3,4,5})
test_map({a=1,b=2,c=3,d=4,e=5})

local function test_copy(t1)
   dumpvalue('test_copy(table)', t1)
   local t2 = table.copy(t1)
   dumpvalue('test_copy(copy)', t2)
   t2.a = 3
   dumpvalue('t1', t1)
   dumpvalue('t2', t2)
end

test_copy({a=1,b=2,c=3,d=4,e=5})
test_copy({a=1,b={c=2,d={e=3}}})


local function test_ifilter(t1)
   dumpvalue('test_ifilter(before)', t1)
   local t2 =table.ifilter(t1, function(v) return v > 3 end)
   dumpvalue('test_ifilter(after)', t2)
end

local function test_filter(t1)
   dumpvalue('test_filter(before)', t1)
   local t2 =table.filter(t1, function(v) return v > 3 end)
   dumpvalue('test_filter(after)', t2)
end

test_ifilter({1,2,3,4,5})
test_filter({1,2,3,4,5})
test_filter({a=1,b=2,c=3,d=4,e=5})

local nestedtable = {
   key1 = 1,
   key2 = { 2, 3 },
   defaults = {
      key1 = { 4, 5 },
      key2 = 6,
      key3 = 7,
      defaults = {
         key1 = { 8 },
         key2 = 9,
         key4 = 10,
      },
   },
}

dumpvalue('nestedtable', nestedtable)

dumpvalue('getfirstvalue(key1)', table.getfirstvalue(nestedtable, 'key1'))
dumpvalue('getfirstvalue(key2)', table.getfirstvalue(nestedtable, 'key2'))
dumpvalue('getfirstvalue(key3)', table.getfirstvalue(nestedtable, 'key3'))
dumpvalue('getfirstvalue(key4)', table.getfirstvalue(nestedtable, 'key4'))

dumpvalue('getallvalues(key1)', table.getallvalues(nestedtable, 'key1'))
dumpvalue('getallvalues(key2)', table.getallvalues(nestedtable, 'key2'))
dumpvalue('getallvalues(key3)', table.getallvalues(nestedtable, 'key3'))
dumpvalue('getallvalues(key4)', table.getallvalues(nestedtable, 'key4'))





